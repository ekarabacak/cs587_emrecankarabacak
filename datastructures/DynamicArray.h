// from CS585 class, my code
// Emre Can Karabacak - 10380370

#ifndef DYNAMICARRAY_H
#define DYNAMICARRAY_H

#include <cstdlib>
#include <assert.h>

/*
 * Template class that can create a vector of any type
 */
template <typename T>
class DynamicArray
{
public:
	DynamicArray<T>(T initVal, unsigned int initSize);		// constructor with initial value and size
	DynamicArray<T>();										// default constructor with default value
	~DynamicArray<T>();										// destructor that destroys the array if it still exists
	T& operator [] (const unsigned int index);				// returns the address for the object being accessed
	bool contains(T obj);									// returns whether the object exists in the array or not
	bool isEmpty();											// returns whether the array has any objects or not
	T remove();												// returns and removes the LAST object in the array
	T removeAt(unsigned int index);							// returns and removes the object at the specified index 
	void removeAll();										// removes all objects
	T poll();												// returns and removes the FIRST object in the array
	T peek();												// returns the FIRST object in the array
	void add(T obj);										// adds the object to the array
	int indexOf(T obj);										// returns the index of the first occurrence of the object, -1 if it doesn't exist
	unsigned int size();									// returns the size of the array
private:
	unsigned int _size;										// size of the array
	unsigned int _internalSize;								// capacity of the array
	T *_array;												// pointer to the array
};

template <typename T>
inline DynamicArray<T>::DynamicArray(T initVal, unsigned int initSize)
{
	assert (initSize > 0);

	_internalSize = initSize;
	_size = 0;

	_array = new T[_internalSize];
	for (unsigned int i = 0; i < _internalSize; i++)
	{
		_array[i] = initVal;
	}
}

template <typename T>
inline DynamicArray<T>::DynamicArray()
{
	_internalSize = 10;
	_size = 0;
	_array = new T[_internalSize];
}

template <typename T>
inline T& DynamicArray<T>::operator [] (const unsigned int index)
{
	if (index < 0)
	{
		return _array[0];
	}
	if (index < _internalSize)
	{
		return _array[index];
	}

	unsigned int oldSize = _internalSize;
	while (index >= _internalSize)
	{
		_internalSize *= 2;
	}

	T* newArray = new T[_internalSize];

	for (unsigned int i = 0; i < oldSize; i++)
	{
		newArray[i] = _array[i];
	}

	delete [] _array;
	_array = newArray;

	return _array[index];
}

template <typename T>
inline unsigned int DynamicArray<T>::size()
{
	return _size;
}

template <typename T>
inline bool DynamicArray<T>::contains(T obj)
{
	for (unsigned int i = 0; i < _size; i++)
	{
		if (_array[i] == obj)
		{
			return true;
		}
	}

	return false;
}

template <typename T>
inline bool DynamicArray<T>::isEmpty()
{
	return (_size == 0);
}

template <typename T>
inline int DynamicArray<T>::indexOf(T obj)
{
	for (unsigned int i = 0; i < _internalSize; i++)
	{
		if (_array[i] == obj)
			return (int)i;
	}
	return -1;
}

template <typename T>
inline void DynamicArray<T>::add(T obj)
{
	if (_size == _internalSize)
	{
		_internalSize *= 2;

		T* newArray = new T[_internalSize];

		for (unsigned int i = 0; i < _size; i++)
		{
			newArray[i] = _array[i];
		}

		delete [] _array;

		_array = newArray;
	}

	_array[_size] = obj;
	_size += 1;
}

template <typename T>
inline T DynamicArray<T>::remove()
{
	assert (_size > 0);

	_size -= 1;

	return _array[_size];
}

template <typename T>
inline T DynamicArray<T>::removeAt(unsigned int index)
{
	T obj = _array[index];
	_array[index] = _array[0];
	_array[0] = obj;

	return poll();
}

template <typename T>
inline void DynamicArray<T>::removeAll()
{
	_size = 0;
}

template <typename T>
inline T DynamicArray<T>::poll()
{
	assert (_size > 0);

	T first = _array[0];

	_size -= 1;
	for (unsigned int i = 0; i < _size; i++)
	{
		_array[i] = _array[i+1];
	}

	return first;
}

template <typename T>
inline T DynamicArray<T>::peek()
{
	return _array[0];
}

template <typename T>
inline DynamicArray<T>::~DynamicArray()
{
	if (_array != nullptr)
	{
		delete [] _array;
	}
}

#endif /* _DYNAMICARRAY_H_ */