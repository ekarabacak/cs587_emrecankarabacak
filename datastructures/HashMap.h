// Emre Can Karabacak 2014 - uses trie nodes
// any exactly 4 digit combination of a-h,0-9; total of 104976 nodes

#ifndef HASHMAP_H
#define HASHMAP_H

#include <string>
#include "DynamicArray.h"

using std::string;

/*
 * Template class that can create a hashmap of any type
 */
template <typename T>
class HashMap
{
public:
	HashMap<T>();							// constructor that creates the fixed size array for use
	~HashMap<T>();							// default destructor that deletes the array if it exists
	T getValue(string key);					// returns the value attached to the given string
	void setValue(string key, T value);		// sets the value for the given string
	bool isSet(string key);					// returns whether the value for the given key exists and is not NULL
private:
	bool isViableKey(string key);			// checks whether the key fits all requirements
	unsigned int getIndex(string key);		// returns the corresponding hashmap index based on the key
	DynamicArray<T> _hashMap;				// array that keeps all hashmap values
};

template <typename T>
inline HashMap<T>::HashMap()
{
	_hashMap = DynamicArray<T>(NULL, 104976);
}

template <typename T>
inline bool HashMap<T>::isViableKey(string key)
{	
	if (key.length() != 4)
	{
		return false;
	}

	char *keyChars = new char[key.length() + 1];
	memcpy(keyChars, key.c_str(), key.length());

	for (int i = 0; i < 4; i++)
	{
		if (!(((keyChars[i] >= 'a') && (keyChars[i] <= 'h'))
			|| ((keyChars[i] >= '0') && (keyChars[i] <= '9'))))
		{
			return false;
		}
	}

	return true;
}

template <typename T>
inline unsigned int HashMap<T>::getIndex(string key)
{
	char *keyChars = new char[key.length() + 1];
	memcpy(keyChars, key.c_str(), key.length());
	
	unsigned int index = 0;

	for (int i = 0; i < 4; i++)
	{
		if (keyChars[i] <= '9')
		{
			index += pow(18, 3 - i)*(8 + keyChars[i] - '0');
		}
		else
		{
			index += pow(18, 3 - i)*(keyChars[i] - 'a');
		}
	}

	return index;
}

template <typename T>
inline T HashMap<T>::getValue(string key)
{
	if (!isViableKey(key))
	{
		return NULL;
	}

	const unsigned int index = getIndex(key);
	
	return _hashMap[index];
}

template <typename T>
inline void HashMap<T>::setValue(string key, T value)
{
	if (!isViableKey(key))
	{
		return;
	}

	unsigned int index = getIndex(key);

	_hashMap[index] = value;
}

template <typename T>
inline HashMap<T>::~HashMap()
{

}

#endif