// Emre Can Karabacak - 2014

#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H

#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <time.h>
#include "externals/mat.h"
#include "externals/LoadShaders.h"
#include "tools/JSONReader.h"
#include "engine/LevelManager.h"
#include "engine/UIManager.h"
#include "engine/ResourceLoader.h"
#include "engine/Renderer.h"
#include "engine/BlackBoard.h"
#include "game/scene/PlayerController.h"
#include "game/scene/ProjectileController.h"
#include "game/scene/GhostController.h"
#include "game/scene/ItemController.h"
#include "game/scene/ActorFactory.h"
#include "engine/physics/Gravity.h"

#define INIT_VIEW_X      0.0
#define INIT_VIEW_Y      0.0
#define INIT_VIEW_Z      0.0
#define VIEW_LEFT        0.0
#define VIEW_RIGHT       500.0
#define VIEW_BOTTOM      0.0
#define VIEW_TOP         300.0
#define VIEW_NEAR        -10.0
#define VIEW_FAR         10.0

class SceneManager
{
public:
	SceneManager();
	~SceneManager();
	void handleEvent(SDL_Event *event);									// handles SDL events
	void tick(float dT);												// updates game components
	void resize();
	void drawScene();													// draws the whole scene and interface
	bool isQuitGame() { return _quitGame; }							
private:
	PlayerController* _player;
	bool _quitGame;
	bool _isMenu;
	JSONReader jsonReader;
	bool _gameOver;
	bool _gameWon;
	void applyMovementInput();											// looks at the variables updated by handleEvent() to change player location
	void checkWinCondition();											// checks if the win conditions are met
	void initData();													// initialize buffers and images
	void initScene();													// reset game components
};

#endif