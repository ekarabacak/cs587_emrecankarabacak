// Emre Can Karabacak - 10380370

#include "Game.h"

int Game::_FPS = 30;

Game::Game()
{
	_display = NULL;
	_quitGame = false;
}

int Game::launch()
{
	if (initialize() == false)
	{
		return -1;
	}

	SDL_Event event;

	glEnable(GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	Uint32 startTick;
	Uint32 endTick = SDL_GetTicks();

	while (!_quitGame)
	{
		startTick = endTick;
		while (SDL_PollEvent(&event))
		{
			eventHandler(&event);
		}
		_quitGame = _scene->isQuitGame();

		while (1000/_FPS > SDL_GetTicks() - startTick)
		{
		}
		
		endTick = SDL_GetTicks();
		_scene->tick((float)(endTick - startTick) / (float)(1000/_FPS));
		_scene->drawScene();		
		
		SDL_GL_SwapWindow(_display);
	}

	return 0;
}

bool Game::initialize()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		printf("SDL initialization failed: %s\n", SDL_GetError());
		return false;
	}

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	if ((_display = SDL_CreateWindow("The Stronghold - Alpha v0.442",
		50, 50, 1000, 600, SDL_WINDOW_OPENGL)) == NULL)
	{
		printf("Could not create window: %s\n", SDL_GetError());
		return false;
	}

	if ((_graphics = SDL_CreateRenderer(_display, -1, 0)) == NULL)
	{
		printf("Could not get renderer: %s\n", SDL_GetError());
		return false;
	}

	if ((_context = SDL_GL_CreateContext(_display)) == NULL)
	{
		printf("Could not get context: %s\n", SDL_GetError());
		return false;
	}
	
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		printf("GLEW couldn't be initialized: %s\n", glewGetErrorString(err));
		return false;
	}

	_scene = new SceneManager();

	return true;
}

void Game::eventHandler(SDL_Event *event)
{
	Uint32 eventType = event->type;

	if (eventType == SDL_QUIT)
	{
		_quitGame = true;
	}
	else
	{
		_scene->handleEvent(event);
	}
}

void Game::quit()
{
	SDL_GL_DeleteContext(_context);
	SDL_DestroyWindow(_display);
	SDL_Quit();
}