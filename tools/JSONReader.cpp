// Emre Can Karabacak - 2014

#include "JSONReader.h"
#include <string>
#include <iostream>
#include <sstream>

using namespace std;

DynamicArray<JSONObject>* JSONReader::_jsonArray = 0;
std::ifstream JSONReader::_json;

DynamicArray<JSONObject>* JSONReader::readJSON(string filePath)
{
	_jsonArray = new DynamicArray<JSONObject>();

	_json.open(filePath);

	if (_json.is_open())
	{
		parseJSON();
	}

	_json.close();

	return _jsonArray;
}

void JSONReader::parseJSON()
{
	char* currentChar = new char[1];
	JSONObject currentObject;
	bool stopReading = false;

	while (!_json.eof() && !stopReading)
	{
		_json.read(currentChar, 1);

		switch (currentChar[0])
		{
		case '{': case ',':
			currentObject = readObject();
			_jsonArray->add(currentObject);
			break;
		case '}':
			stopReading = true;
			break;
		}
	}
}

JSONObject JSONReader::readObject()
{
	char* currentChar = new char[1];
	string currentKey;
	DynamicArray<JSONObject>* wholeInnerObject = new DynamicArray<JSONObject>();
	JSONObject currentObject;

	// get to key
	while (currentChar[0] != '\"')
	{
		_json.read(currentChar, 1);
	}

	// read the key
	currentKey.clear();
	_json.read(currentChar, 1);
	while (currentChar[0] != '\"')
	{
		currentKey.push_back(currentChar[0]);
		_json.read(currentChar, 1);
	}
	
	currentObject._jsonKey = currentKey;
		
	// get to value
	while (currentChar[0] != ':')
	{
		_json.read(currentChar, 1);
	}
	_json.read(currentChar, 1);
	
	while ((currentChar[0] == ' ') || (currentChar[0] == '\n') || (currentChar[0] == '\t'))
	{
		currentChar[0] = _json.peek();
		if ((currentChar[0] == '{') || (currentChar[0] == '[') || (currentChar[0] == ' ') || (currentChar[0] == '\n') || (currentChar[0] == '\t'))
		{
			_json.read(currentChar, 1);
		}
		else
		{
			currentChar[0] = 'V';
		}
	}
	
	// read the value
	JSONObject innerObject;
	DynamicArray<JSONObject::JSONValue>* valueArray; 
	if (currentChar[0] == '{')
	{
		innerObject = readObject();
		wholeInnerObject->add(innerObject);
		while ((currentChar[0] != '}') && !_json.eof())
		{
			if (currentChar[0] == ',')
			{
				innerObject = readObject();
				wholeInnerObject->add(innerObject);
			}
			_json.read(currentChar, 1);
		}
		currentObject._jsonValue.pointer = wholeInnerObject;
		currentObject._valueType = JSONObject::ValueType::JSONVALUE_POINTER;
	}
	else if (currentChar[0] == '[')
	{
		valueArray = readArray();
		currentObject._jsonValue.array = valueArray;
		currentObject._valueType = JSONObject::ValueType::JSONVALUE_ARRAY;
	}
	else
	{
		readValue(currentObject._jsonValue, currentObject._valueType);
	}
	
	return currentObject;
}

DynamicArray<JSONObject::JSONValue>* JSONReader::readArray()
{	
	char* currentChar = new char[1];
	string stringValue = "";

	JSONObject::ValueType type;
	JSONObject::JSONValue* currValue = new JSONObject::JSONValue;

	DynamicArray<JSONObject::JSONValue>* valueArray = new DynamicArray<JSONObject::JSONValue>();

	currentChar[0] = _json.peek();
	while ((!_json.eof()) && ((currentChar[0] != ',') && (currentChar[0] != ']')))
	{
		_json.read(currentChar, 1);
		if (currentChar[0] != ' ')
		{
			stringValue.push_back(currentChar[0]);
		}
		currentChar[0] = _json.peek();
	}

	if (isInteger(stringValue))
	{
		type = JSONObject::ValueType::JSONVALUE_INTEGER;
		currValue->iValue = atoi(stringValue.data());
	}
	else if (isFloat(stringValue))
	{
		type = JSONObject::ValueType::JSONVALUE_FLOAT;
		currValue->fValue = atof(stringValue.data());
	}
	else if (isBoolean(stringValue))
	{
		type = JSONObject::ValueType::JSONVALUE_BOOLEAN;
		if (stringValue.compare("true"))
		{
			currValue->bValue = true;
		}
		else
		{
			currValue->bValue = false;
		}
	}
	else if (isChar(stringValue))
	{
		type = JSONObject::ValueType::JSONVALUE_CHAR;
		currValue->cValue = stringValue.substr(1, stringValue.length() - 2).data()[0];
	}
	else
	{
		type = JSONObject::ValueType::JSONVALUE_STRING;
		currValue->string = &stringValue.substr(1, stringValue.length() - 2)[0];
	}
	
	valueArray->add(*currValue);

	_json.read(currentChar, 1);
	while ((!_json.eof()) && (currentChar[0] != ']'))
	{
		stringValue.clear();
		
		currentChar[0] = _json.peek();
		while ((!_json.eof()) && ((currentChar[0] != ',') && (currentChar[0] != ']')))
		{
			_json.read(currentChar, 1);
			if (currentChar[0] != ' ')
			{
				stringValue.push_back(currentChar[0]);
			}
			currentChar[0] = _json.peek();
		}

		currValue = new JSONObject::JSONValue();

		if (type == JSONObject::ValueType::JSONVALUE_INTEGER)
		{
			currValue->iValue = atoi(stringValue.data());
		}
		else if (type == JSONObject::ValueType::JSONVALUE_FLOAT)
		{
			currValue->fValue = atof(stringValue.data());
		}
		else if (type == JSONObject::ValueType::JSONVALUE_BOOLEAN)
		{
			if (stringValue.compare("true"))
			{
				currValue->bValue = true;
			}
			else
			{
				currValue->bValue = false;
			}
		}
		else if (type == JSONObject::ValueType::JSONVALUE_CHAR)
		{
			currValue->cValue = stringValue.substr(1, stringValue.length() - 2).data()[0];
		}
		else
		{
			currValue->string = &stringValue.substr(1, stringValue.length() - 2)[0];
		}

		valueArray->add(*currValue);
	}
	
	return valueArray;
}

void JSONReader::readValue(JSONObject::JSONValue& value, JSONObject::ValueType& type)
{	
	char* currentChar = new char[1];
	string stringValue = "";
	
	currentChar[0] = _json.peek();
	while ((!_json.eof()) && ((currentChar[0] != '}') && (currentChar[0] != ',')))
	{
		_json.read(currentChar, 1);
		if ((currentChar[0] != '\n') && (currentChar[0] != '\t'))
		{
			stringValue.push_back(currentChar[0]);
		}
		currentChar[0] = _json.peek();
	}

	if (isInteger(stringValue))
	{
		value.iValue = atoi(stringValue.data());
		type = JSONObject::ValueType::JSONVALUE_INTEGER;
	}
	else if (isFloat(stringValue))
	{
		value.fValue = atof(stringValue.data());
		type = JSONObject::ValueType::JSONVALUE_FLOAT;
	}
	else if (isBoolean(stringValue))
	{
		if (stringValue.compare("true"))
		{
			value.bValue = true;
		}
		else
		{
			value.bValue = false;
		}
		type = JSONObject::ValueType::JSONVALUE_BOOLEAN;
	}
	else if (isChar(stringValue))
	{
		value.cValue = stringValue.substr(1, stringValue.length() - 2).data()[0];
		type = JSONObject::ValueType::JSONVALUE_CHAR;
	}
	else
	{
		value.string = &stringValue.substr(1, stringValue.length() - 2)[0];
		type = JSONObject::ValueType::JSONVALUE_STRING;
	}
	return;
}

bool JSONReader::isInteger(string stringValue)
{
	istringstream iss(stringValue);
    int i;
    iss >> noskipws >> i; 
    return iss.eof() && !iss.fail(); 
}

bool JSONReader::isFloat(string stringValue)
{
	istringstream iss(stringValue);
    float f;
    iss >> noskipws >> f; 
    return iss.eof() && !iss.fail(); 
}

bool JSONReader::isBoolean(string stringValue)
{
	istringstream iss(stringValue);
    bool b;
    iss >> noskipws >> b; 
    return iss.eof() && !iss.fail(); 
}

bool JSONReader::isChar(string stringValue)
{
	if (stringValue.length() == 3)
	{
		return true;
	} 
	else
	{
		return false;
	}
}