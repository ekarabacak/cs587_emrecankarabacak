// Emre Can Karabacak - 10380370

#ifndef JSONOBJECT_H
#define JSONOBJECT_H

#include "../datastructures/DynamicArray.h"
#include <string>

struct JSONObject
{
	union JSONValue
	{
		bool bValue;
		int iValue;
		float fValue;
		char cValue;
		char* string;
		DynamicArray<JSONValue>* array;
		DynamicArray<JSONObject>* pointer;
	} _jsonValue;
	std::string _jsonKey;
	enum ValueType
	{
		JSONVALUE_BOOLEAN,
		JSONVALUE_INTEGER,
		JSONVALUE_FLOAT,
		JSONVALUE_CHAR,
		JSONVALUE_STRING,
		JSONVALUE_ARRAY,
		JSONVALUE_POINTER
	} _valueType;
	
	std::string toString()
	{
		std::string finalString = "";
		switch (_valueType)
		{
		case JSONVALUE_BOOLEAN:
			finalString.append("BOOL: ");
			finalString.append(_jsonKey);
			finalString.append(" = ");
			if (_jsonValue.bValue)
			{
				finalString.append("TRUE");
			}
			else
			{
				finalString.append("FALSE");
			}
			break;
		case JSONVALUE_INTEGER:
			finalString.append("INT: ");			
			finalString.append(_jsonKey);
			finalString.append(" = ");
			finalString.append(std::to_string(_jsonValue.iValue));
			break;
		case JSONVALUE_FLOAT:
			finalString.append("FLOAT: ");
			finalString.append(_jsonKey);
			finalString.append(" = ");
			finalString.append(std::to_string(_jsonValue.fValue));
			break;			
		case JSONVALUE_CHAR:
			finalString.append("CHAR: ");
			finalString.append(_jsonKey);
			finalString.append(" = ");
			finalString.push_back(_jsonValue.cValue);
			break;
		case JSONVALUE_STRING:
			finalString.append("STRING: ");
			finalString.append(_jsonKey);
			finalString.append(" = ");
			finalString.append(_jsonValue.string);
			break;
		case JSONVALUE_ARRAY:
			finalString.append("ARRAY: ");
			finalString.append(_jsonKey);
			finalString.append(" = [");
			for (unsigned int i = 0; i < _jsonValue.array->size(); i++)
			{
				finalString.append(std::to_string(_jsonValue.array[0][i].iValue));
				if (i == _jsonValue.array->size()-1)
				{
					finalString.append("]");
				}
				else
				{
					finalString.append(", ");
				}
			}
			break;
		case JSONVALUE_POINTER:
			finalString.append("OBJECT: ");
			finalString.append(_jsonKey);
			finalString.append(" = {");
			finalString.push_back('\n');
			for (unsigned int i = 0; i < _jsonValue.pointer->size(); i++)
			{
				finalString.append(_jsonValue.pointer[0][i].toString());
				finalString.push_back('\n');
			}
			finalString.append("}");
			break;
		}

		return finalString;
	};
};

#endif