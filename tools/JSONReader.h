// Emre Can Karabacak - 2014

#ifndef JSONREADER_H
#define JSONREADER_H

#include "JSONObject.h"
#include <fstream>

class JSONReader
{
public:
	static DynamicArray<JSONObject>* readJSON(std::string filePath);
private:
	static void parseJSON();
	static JSONObject readObject();
	static DynamicArray<JSONObject::JSONValue>* readArray();
	static void readValue(JSONObject::JSONValue& value, JSONObject::ValueType& type);
	static bool isInteger(std::string stringValue);
	static bool isFloat(std::string stringValue);
	static bool isBoolean(std::string stringValue);
	static bool isChar(std::string stringValue);
	static DynamicArray<JSONObject>* _jsonArray;
	static std::ifstream _json;
};

#endif