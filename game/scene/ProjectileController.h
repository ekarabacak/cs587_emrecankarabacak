// Emre Can Karabacak - 10380370

#ifndef PROJECTILECONTROLLER_H
#define PROJECTILECONTROLLER_H

#include "Controller.h"
#include "GhostController.h"
#include "../../engine/LevelManager.h"

class ProjectileController : public Controller
{
public:
	ProjectileController(Actor* projectile) : Controller(projectile) { }	
	~ProjectileController() { }	
	virtual void tick(float dT);
private:
};

#endif