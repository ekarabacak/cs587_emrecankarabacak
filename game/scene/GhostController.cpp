// Emre Can Karabacak - 2014

#include "GhostController.h"

void GhostController::tick(float dT)
{
	Point* p = BlackBoard::getInstance().getPlayer()->getLocation();
	Point* g = getActor()->getLocation();
	Velocity* v = getActor()->getVelocity();

	if (sqrt(pow(p->getX() - g->getX(), 2) + pow(p->getY() - g->getY(), 2)) < 300)
	{
		if (p->getX() > g->getX())	
		{
			v->setHSpeed(0.7f);
		}
		else
		{
			v->setHSpeed(-0.7f);
		}
	
		if (p->getY() > g->getY())
		{
			v->setVSpeed(0.7f);
		}
		else
		{
			v->setVSpeed(-0.7f);
		}
	}
	else
	{
		v->setHSpeed(0.0f);
		v->setVSpeed(0.0f);
	}

	g->setX(g->getX() + v->getHSpeed() * dT);
	g->setY(g->getY() + v->getVSpeed() * dT);
}