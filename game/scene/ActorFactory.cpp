// Emre Can Karabacak - 2014

#include "ActorFactory.h"
#include "PlayerActor.h"

unsigned int ActorFactory::_ID = 0;

Actor* ActorFactory:: getActor(float x, float y, float z, string name, int health, int type)
{
	Actor* actor;
	PlayerActor* player;

	if (type == ActorType::TypePlayer)
	{
		player = new PlayerActor(_ID++, x, y, z, name, health, type);

		return player;
	}
	else
	{
		actor = new Actor(_ID++, x, y, z, name, health, type);

		return actor;
	}
}