// Emre Can Karabacak - 2014

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "Actor.h"
#include "../../engine/interfaces/ITickable.h"
#include "../../engine/BlackBoard.h"

class Controller : public ITickable
{
public:
	Controller(Actor* actor) { _actor = actor; }
	~Controller() { delete _actor; }	
	Actor* getActor() { return _actor; }
	virtual void tick(float dT) = 0;
private:
	Actor* _actor;
};

#endif