// Emre Can Karabacak - 2014

#include "ProjectileController.h"

void ProjectileController::tick(float dT)
{
	Velocity* vel = getActor()->getVelocity();
	Point* p = getActor()->getLocation();

	p->setX(p->getX() + vel->getHSpeed() * dT);
	p->setY(p->getY() + vel->getVSpeed() * dT);

	int row = LevelManager::getInstance().getHeight() - 2 - (p->getY() - 32.0f) / 32;
	int col = (p->getX()-32.0f) / 32;


	if (p->getX() <= 32.0f || p->getX() >= (LevelManager::getInstance().getWidth() - 1)*32.0f 
		|| p->getY() <= 32.0f || p->getY() >= (LevelManager::getInstance().getHeight() - 1)*32.0f)
	{
		getActor()->setDead(true);
		return;
	}

	int tile = LevelManager::getInstance().getLevelMap()[(row+1) * LevelManager::getInstance().getWidth() + col + 1];
	if (tile > 0)
	{
		LevelManager::getInstance().getLevelMap()[(row+1) * LevelManager::getInstance().getWidth() + col + 1] = -tile;

		if (rand() % 100 > 70)
		{
			LevelManager::getInstance().getActorArray()->add(
				new GhostController(ActorFactory::getInstance().getActor((col+1) * 32,
				(LevelManager::getInstance().getHeight() - row - 2) * 32, 0.0f, "Ghost", 100, ActorType::TypeNPC)));
		}

		getActor()->setDead(true);
		return;
	}

	if (row >= 0 && row < LevelManager::getInstance().getHeight()-2 
		&& col >= 0 && col < LevelManager::getInstance().getWidth()-2)
	{
		DynamicArray<Actor*>* a = LevelManager::getInstance().getActorMap()[0][row * (LevelManager::getInstance().getWidth()-2) + col];
			
		if (!a->isEmpty())
		{
			for (int m = 0; m < a->size(); m++)
			{	
				Actor* actor = a[0][m];

				if (actor->getType() != ActorType::TypeNPC)
				{
					continue;
				}

				Point* aPoint = actor->getLocation();

				if (((p->getX() <= aPoint->getX() + 20.0f) && (p->getX() >= aPoint->getX()))
					|| ((p->getX() + 3.0f <= aPoint->getX() + 20.0f) && (p->getX() + 3.0f >= aPoint->getX())))
				{
					if (((p->getY() <= aPoint->getY() + 20.0f) && (p->getY() >= aPoint->getY()))
						|| ((p->getY() + 3.0f <= aPoint->getY() + 20.0f) && (p->getY() + 3.0f >= aPoint->getY())))
					{
						actor->setDead(true);
						getActor()->setDead(true);
						return;
					}
				}
			}
		}
	}
}