// Emre Can Karabacak - 10380370

#ifndef PLAYERCONTROLLER_H
#define PLAYERCONTROLLER_H

#include "Controller.h"
#include "PlayerActor.h"
#include "../../engine/LevelManager.h"
#include "../../engine/UIManager.h"
#include "../../engine/physics/Gravity.h"

class PlayerController : public Controller
{
public:
	PlayerController(Actor* player) : Controller(player) { _player = (PlayerActor*)player; }	
	PlayerActor* getActor() { return _player; }
	~PlayerController() { delete _player; }	
	virtual void tick(float dT);
private:
	PlayerActor* _player;
};

#endif