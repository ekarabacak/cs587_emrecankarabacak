// Emre Can Karabacak - 10380370

#ifndef ITEMCONTROLLER_H
#define ITEMCONTROLLER_H

#include "Controller.h"
#include "../../engine/UIManager.h"

class ItemController : public Controller
{
public:
	ItemController(Actor* item) : Controller(item) { _item = item; }	
	Actor* getActor() { return _item; }
	~ItemController() { delete _item; }	
	virtual void tick(float dT);
private:
	Actor* _item;
};

#endif