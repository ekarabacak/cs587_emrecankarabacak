// Emre Can Karabacak - 10380370

#include "PlayerController.h"

void PlayerController::tick(float dT)
{
	Velocity* pVel = getActor()->getVelocity();

	if (getActor()->getPlayerAir() != 1)
	{
		pVel->setVSpeed(pVel->getVSpeed() - Gravity::getInstance().getGravity());
	}

	Point* pPos = getActor()->getLocation();

	if (pVel->getHSpeed() > 0)
	{
		if (LevelManager::getInstance().hasMapCollision(pPos->getX() + pVel->getHSpeed(), pPos->getY(), 12, 20))
		{
			pPos->setX(pPos->getX() + pVel->getHSpeed() - ((int)(pPos->getX() + pVel->getHSpeed() + 12.0f) % 32));
			pVel->setHSpeed(0.0f);
		}
		pPos->setX(pPos->getX() + pVel->getHSpeed() * dT);
	}
	else if (pVel->getHSpeed() < 0)
	{
		if (LevelManager::getInstance().hasMapCollision(pPos->getX() + pVel->getHSpeed(), pPos->getY(), 12, 20))
		{
			pPos->setX(pPos->getX() + pVel->getHSpeed() + (32 - ((int)(pPos->getX() + pVel->getHSpeed()) % 32)));
			pVel->setHSpeed(0.0f);
		}
		pPos->setX(pPos->getX() + pVel->getHSpeed() * dT);
	}

	if (pVel->getVSpeed() > 0)
	{
		if (LevelManager::getInstance().hasMapCollision(pPos->getX(), pPos->getY() + pVel->getVSpeed(), 12, 20))
		{
			pPos->setY(pPos->getY() + pVel->getVSpeed() - ((int)(pPos->getY() + pVel->getVSpeed() + 20.0f) % 32));
			pVel->setVSpeed(0.0f);
		}
		pPos->setY(pPos->getY() + pVel->getVSpeed() * dT);
	}
	else if (pVel->getVSpeed() < 0)
	{
		if (LevelManager::getInstance().hasMapCollision(pPos->getX(), pPos->getY() + pVel->getVSpeed(), 12, 20))
		{
			pPos->setY(((int)(pPos->getY() + pVel->getVSpeed() + 31) / 32) * 32);
			pVel->setVSpeed(0.0f);
		}
		pPos->setY(pPos->getY() + pVel->getVSpeed() * dT);
	}
	
	int row = LevelManager::getInstance().getHeight() - 2 - (pPos->getY() - 32.0f) / 32;
	int col = (pPos->getX() - 32.0f) / 32;

	DynamicArray<Actor*>* a = LevelManager::getActorMap()[0][row * (LevelManager::getInstance().getWidth()-2) + col];

	if (!a->isEmpty())
	{
		for (int m = 0; m < a->size(); m++)
		{
			Actor* actor = a[0][m];
			if (actor->getType() == ActorType::TypeProjectile)
			{
				continue;
			}
			else if (actor->getType() == ActorType::TypeNPC)
			{
				Point* aPoint = actor->getLocation();

				if (((pPos->getX() <= aPoint->getX() + 20.0f) && (pPos->getX() >= aPoint->getX()))
					|| ((pPos->getX() + 12.0f <= aPoint->getX() + 20.0f) && (pPos->getX() + 12.0f >= aPoint->getX())))
				{
					if (((pPos->getY() <= aPoint->getY() + 20.0f) && (pPos->getY() >= aPoint->getY()))
						|| ((pPos->getY() + 20.0f <= aPoint->getY() + 20.0f) && (pPos->getY() + 20.0f >= aPoint->getY())))
					{
						actor->setDead(true);
						getActor()->setHealth(getActor()->getHealth() - 10);
						if (getActor()->getHealth() <= 0)
						{
							getActor()->setDead(true);
						}
					}
				}
			}
			else if (actor->getType() == ActorType::TypeAmmo)
			{
				Point* aPoint = actor->getLocation();

				if (((pPos->getX() <= aPoint->getX() + 20.0f) && (pPos->getX() >= aPoint->getX()))
					|| ((pPos->getX() + 12.0f <= aPoint->getX() + 20.0f) && (pPos->getX() + 12.0f >= aPoint->getX())))
				{
					if (((pPos->getY() <= aPoint->getY() + 20.0f) && (pPos->getY() >= aPoint->getY()))
						|| ((pPos->getY() + 20.0f <= aPoint->getY() + 20.0f) && (pPos->getY() + 20.0f >= aPoint->getY())))
					{
						actor->setDead(true);
						UIManager::getInstance().setAmmo(10);
					}
				}
			}
		}
	}
}