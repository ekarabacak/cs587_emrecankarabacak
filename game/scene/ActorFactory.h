// Emre Can Karabacak - 10380370

#ifndef ACTORFACTORY_H
#define ACTORFACTORY_H

#include "Actor.h"
#include <string>

using std::string;

class ActorFactory
{
public:
	static ActorFactory& getInstance()
	{
		static ActorFactory instance;
		return instance;
	}
	Actor* getActor(float x, float y, float z, string name, int health, int type);
private:
	ActorFactory() { }
	ActorFactory(ActorFactory const&);
	void operator=(ActorFactory const&);
	static unsigned int _ID;
};

#endif