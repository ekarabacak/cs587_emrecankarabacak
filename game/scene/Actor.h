// Emre Can Karabacak - 2014

#ifndef ACTOR_H
#define ACTOR_H

enum ActorType { TypePlayer, TypeNPC, TypeAmmo, TypeProjectile };

#include <string>
#include "properties/Point.h"
#include "properties/Velocity.h"

using std::string;

class Actor
{
public:
	Actor(unsigned int ID, float x, float y, float z, string name, int health, int type)
	{ _ID = ID; _loc = new Point(x, y, z); _vel = new Velocity(); _name = name; _health = health; _type = type; _isDead = false; }
	~Actor() { delete _vel; delete _loc; }
	unsigned int getID() { return _ID; }
	int getType() { return _type; }
	string getName() { return _name; }
	Point* getLocation() { return _loc; }
	Velocity* getVelocity() { return _vel; }
	int getHealth() { return _health; }
	void setHealth(int health) { _health = health; }
	bool isDead() { return _isDead; }
	void setDead(bool isDead) { _isDead = isDead; }
protected:
	int _type;
	int _health;
	string _name;
	Point* _loc;
	Velocity* _vel;
	bool _isDead;
private:
	unsigned int _ID;
};

#endif