// Emre Can Karabacak - 2014

#ifndef GHOSTCONTROLLER_H
#define GHOSTCONTROLLER_H

#include "Controller.h"

class GhostController : public Controller
{
public:
	GhostController(Actor* ghost) : Controller(ghost) { }
	~GhostController() { }	
	virtual void tick(float dT);
private:
};

#endif