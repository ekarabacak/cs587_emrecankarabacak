#ifndef POINT_H
#define POINT_H

class Point
{
public:
	Point(): _x(0.0f), _y(0.0f), _z(0.0f) { }
	Point(float x, float y, float z): _x(x), _y(y), _z(z) { }
	float getX() { return _x; }
	float getY() { return _y; }
	float getZ() { return _z; }
	void setX(float x) { _x = x; }
	void setY(float y) { _y = y; }
	void setZ(float z) { _z = z; }
private:
	float _x;
	float _y;
	float _z;
};

#endif