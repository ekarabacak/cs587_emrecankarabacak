// Emre Can Karabacak - 10380370

#ifndef VELOCITY_H
#define VELOCITY_H

class Velocity
{
public:
	Velocity() { _hSpeed = 0.0f; _vSpeed = 0.0f; }
	Velocity(float value) { _hSpeed = value; _vSpeed = value; }
	Velocity(float hSpeed, float vSpeed) { _hSpeed = hSpeed; _vSpeed = vSpeed; }
	float getHSpeed() { return _hSpeed; }
	float getVSpeed() { return _vSpeed; }
	void setHSpeed(float hSpeed) { _hSpeed = hSpeed; }
	void setVSpeed(float vSpeed) 
	{
		if (vSpeed < -5.0f)
		{
			_vSpeed = -5.0f;
		}
		else if (vSpeed > 5.0f)
		{
			_vSpeed = 5.0f;
		}
		else
		{
			_vSpeed = vSpeed;
		}
	}
private:
	float _hSpeed;
	float _vSpeed;
};

#endif