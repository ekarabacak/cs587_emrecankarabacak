// Emre Can Karabacak - 10380370

#ifndef PLAYERACTOR_H
#define PLAYERACTOR_H

#include "Actor.h"

class PlayerActor : public Actor
{
public:
	PlayerActor(unsigned int ID, float x, float y, float z, string name, int health, int type)
		: Actor(ID, x, y, z, name, health, type) { _playerDirection =_playerAir = _playerMoving = 0; }
	int getPlayerDirection() { return _playerDirection; }
	int getPlayerAir() { return _playerAir; }
	int getPlayerMoving() { return _playerMoving; }
	void setPlayerDirection(int playerDirection) { _playerDirection = playerDirection; }
	void setPlayerAir(int playerAir) { _playerAir = playerAir; }
	void setPlayerMoving(int playerMoving) { _playerMoving = playerMoving; } 
private:
	int _playerDirection;
	int _playerMoving;
	int _playerAir;
};

#endif