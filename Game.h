// Emre Can Karabacak - 10380370

#ifndef CS587_EMRECANKARABACAK
#define CS587_EMRECANKARABACAK

#include "SceneManager.h"

class Game
{
public:
	Game();
	int launch();
	bool initialize();
	void eventHandler(SDL_Event *event);
	void quit();
private:
	static int _FPS;
	SDL_Window* _display;
	SDL_Renderer* _graphics;
	SDL_GLContext _context;
	SceneManager* _scene;
	bool _quitGame;
};

#endif