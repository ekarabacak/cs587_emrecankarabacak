// Emre Can Karabacak - 10380370

#ifndef LEVELMANAGER_H
#define LEVELMANAGER_H

#include <cstdlib>
#include "../datastructures/DynamicArray.h"
#include "../tools/JSONObject.h"
#include "../game/scene/Actor.h"
#include "../game/scene/Controller.h"
#include "../game/scene/GhostController.h"
#include "../game/scene/ItemController.h"
#include "../game/scene/ActorFactory.h"

class LevelManager
{
public:
	static LevelManager& getInstance()
	{
		static LevelManager instance;
		return instance;
	}
	static void initializeLevel(DynamicArray<JSONObject>* json);
	static int getLevel() { return _level; }
	static int getWidth() { return _width; }
	static int getHeight() { return _height; }
	static int getSkybox() { return _skybox; }
	static int getSecondsLeft() { return _secondsLeft; }
	static int* getLevelMap() { return _levelMap; }
	static void updateActorMap(); 
	static void updateTime(float dT);
	static bool hasMapCollision(float x, float y, float width, float height);
	static DynamicArray<Controller*>* getActorArray() { return _actorArray; }
	static DynamicArray<DynamicArray<Actor*>*>* getActorMap() { return _actorMap; }
private:
	LevelManager() { }
	LevelManager(LevelManager const&);
	void operator=(LevelManager const&);
	static DynamicArray<Controller*>* _actorArray;
	static DynamicArray<DynamicArray<Actor*>*>* _actorMap;
	static bool hasConnection(); 
	static unsigned int _level;
	static int _width;
	static int _height;
	static int _skybox;
	static int* _levelMap;
	static int _secondsLeft;
	static float _ticks;
};

#endif