// Emre Can Karabacak - 2014

#ifndef BLACKBOARD_H
#define BLACKBOARD_H

#include "../game/scene/PlayerActor.h"

class BlackBoard
{
public:
	static BlackBoard& getInstance()
	{
		static BlackBoard instance;
		return instance;
	}
	static void updatePlayer(PlayerActor* player) { _player = player; }
	static PlayerActor* getPlayer() { return _player; }
	static void setGameOver(bool gameOver) { _gameOver = gameOver; }
	static bool isGameOver() { return _gameOver; }
	static void setGameWon(bool gameWon) { _gameWon = gameWon; }
	static bool isGameWon() { return _gameWon; }
	static void setMenu(bool isMenu) { _isMenu = isMenu; }
	static bool isMenu() { return _isMenu; }
	static void setMouseONG(bool mouseONG) { _mouseOverNewGame = mouseONG; }
	static bool isMouseONG() { return _mouseOverNewGame; }
	static void setMouseOQG(bool mouseOQG) { _mouseOverQuitGame = mouseOQG; }
	static bool isMouseOQG() { return _mouseOverQuitGame; }
private:
	BlackBoard() { }
	BlackBoard(BlackBoard const&);
	void operator=(BlackBoard const&);
	static PlayerActor* _player;
	static bool _gameOver;
	static bool _gameWon;
	static bool _isMenu;
	static bool _mouseOverNewGame;
	static bool _mouseOverQuitGame;
};

#endif