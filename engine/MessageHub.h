// Emre Can Karabacak - 2014

#ifndef MESSAGEHUB_H
#define MESSAGEHUB_H

#include <string>
#include "interfaces/ISubscriber.h"
#include "../datastructures/DynamicArray.h"

class MessageHub 
{
public:
	static MessageHub& getInstance()
	{
		static MessageHub instance;
		return instance;
	}
	static void publishMessage(std::string content, std::string message);
private:
	MessageHub() { }
	MessageHub(MessageHub const&);
	void operator=(MessageHub const&);
	static DynamicArray<std::string>* _contents;
	static DynamicArray<DynamicArray<ISubscriber*>>* _subscribers;
};

#endif