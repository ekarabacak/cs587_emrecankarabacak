// Emre Can Karabacak - 2014

#ifndef RENDERER_H
#define RENDERER_H

#include <GL/glew.h>
#include "LevelManager.h"
#include "ResourceLoader.h"
#include "BlackBoard.h"
#include "../externals/mat.h"
#include "../externals/LoadShaders.h"

#define INIT_VIEW_X      0.0
#define INIT_VIEW_Y      0.0
#define INIT_VIEW_Z      0.0
#define VIEW_LEFT        0.0
#define VIEW_RIGHT       500.0
#define VIEW_BOTTOM      0.0
#define VIEW_TOP         300.0
#define VIEW_NEAR        -10.0
#define VIEW_FAR         10.0

class Renderer
{
public:
	static Renderer& getInstance()
	{
		static Renderer instance;
		return instance;
	}
	static void renderScene();
	static void initiateRenderer();
	static void updateRenderer();
private:
	Renderer() { }
	Renderer(Renderer const&);
	void operator=(Renderer const&);
	static void drawSkybox();													// renders skybox
	static void drawMap();														// renders map
	static void drawPlayer();													// renders player
	static void drawProjectile(Actor* actor);									// renders projectiles
	static void drawUI();														// renders user interface
	static void drawMenu();														// renders menu
	static void drawActor(Actor* actor);										// renders NPCs
	static void drawDecals();													// renders start and end portals
};

#endif