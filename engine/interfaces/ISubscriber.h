// Emre Can Karabacak - 2014

#ifndef ISUBSCRIBER_H
#define ISUBSCRIBER_H

#include <string>
#include "../../datastructures/DynamicArray.h"

class ISubscriber
{
public:
	virtual void subscribe(DynamicArray<std::string> content) = 0;
	virtual void receiveMessage(std::string content, std::string message) = 0;
	virtual ~ISubscriber() { }
};

#endif