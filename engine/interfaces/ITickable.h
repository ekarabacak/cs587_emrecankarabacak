// Emre Can Karabacak - 2014

#ifndef ITICKABLE_H
#define ITICKABLE_H

class ITickable
{
public:
	virtual void tick(float dT) = 0;
	virtual ~ITickable() { }
};

#endif