// Emre Can Karabacak - 10380370

#ifndef RESOURCELOADER_H
#define RESOURCELOADER_H

#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>
#include "LevelManager.h"
#include "../externals/lodepng.h"
#include "../externals/mat.h"

class ResourceLoader
{
public:
	static ResourceLoader& getInstance()
	{
		static ResourceLoader instance;
		return instance;
	}
	static void loadAllImages(GLuint*& textures);
	static void loadSceneComponents();
	static void loadAllBuffers(GLuint texShader, GLuint*& vElements, GLuint*& vArrays, GLuint*& vBuffers);
	static void clearAllocations();
private:
	ResourceLoader() { }
	ResourceLoader(ResourceLoader const&);
	void operator=(ResourceLoader const&);
	static void loadImage(GLuint*& textures, const char* filename, int index);
};

#endif