// Emre Can Karabacak - 2014

#include "LevelManager.h"
#include <iostream>

using namespace std;

unsigned int LevelManager::_level = 0;
int LevelManager::_width = 0;
int LevelManager::_height = 0;
int LevelManager::_skybox = 0;
int* LevelManager::_levelMap = 0;
int LevelManager::_secondsLeft = 120;
float LevelManager::_ticks = 0.0;
DynamicArray<Controller*>* LevelManager::_actorArray =  new DynamicArray<Controller*>();
DynamicArray<DynamicArray<Actor*>*>* LevelManager::_actorMap = new DynamicArray<DynamicArray<Actor*>*>();

void LevelManager::updateActorMap()
{	
	_actorMap = new DynamicArray<DynamicArray<Actor*>*>(new DynamicArray<Actor*>(), (LevelManager::getInstance().getHeight()-1) *
		(LevelManager::getInstance().getWidth()-1));

	for (int i = 0; i < _actorArray->size(); i++)
	{
		Point* pActor = _actorArray[0][i]->getActor()->getLocation();
		int row = LevelManager::getInstance().getHeight() - 2 - (pActor->getY()-32.0f) / 32;
		int col = (pActor->getX()-32.0f) / 32;

		_actorMap[0][row * (LevelManager::getInstance().getWidth() - 2) + col]->add(_actorArray[0][i]->getActor());
	}
}

void LevelManager::updateTime(float dT)
{
	_ticks += dT;
	if (_ticks >= 30.0)
	{
		_ticks = _ticks - 30.0;
		if (_secondsLeft != 0)
		{
			_secondsLeft--;
		}
	}
}

void LevelManager::initializeLevel(DynamicArray<JSONObject>* json)
{
	_level++;
	DynamicArray<JSONObject>* levelInfo;
	DynamicArray<JSONObject>* levelMap;
	_actorArray =  new DynamicArray<Controller*>();
	_secondsLeft = 120;
	_ticks = 0;

	for (int i = 0; i < json->size(); i++)
	{
		if (json[0][i]._jsonKey == "level")
		{
			levelInfo = json[0][i]._jsonValue.pointer;
		}
	}

	for (int i = 0; i < levelInfo->size(); i++)
	{
		if (levelInfo[0][i]._jsonKey == "map")
		{
			levelMap = levelInfo[0][i]._jsonValue.pointer;
		}
	}

	for (int i = 0; i < levelMap->size(); i++)
	{
		if (levelMap[0][i]._jsonKey == "width")
		{
			_width = levelMap[0][i]._jsonValue.iValue;
		}
		else if (levelMap[0][i]._jsonKey == "height")
		{
			_height = levelMap[0][i]._jsonValue.iValue;
		}
		else if (levelMap[0][i]._jsonKey == "skybox")
		{
			_skybox = levelMap[0][i]._jsonValue.iValue;
		}
	}
	
	_levelMap = new int[_width * _height];

	// randomly generate the map
	for (int i = 0; i < 20; i++)
	{
		for (int j = 0; j < _width; j++)
		{
			if (rand() % 10 < 7)
			{
				_levelMap[i * _width + j] = rand() % 4 + 1;
			}
			else
			{
				_levelMap[i * _width + j] = rand() % 5 + 5;
			}
		}
	}

	for (int j = 0; j < _width; j++)
	{
		_levelMap[20 * _width + j] = 1;
	}

	for (int j = 0; j < _width; j++)
	{
		_levelMap[21 * _width + j] = 10;
	}

	for (int i = 22; i < 40; i++)
	{
		for (int j = 0; j < _width; j++)
		{
			if (rand() % 10 < 6)
			{
				_levelMap[i * _width + j] = 11;
			}
			else
			{
				_levelMap[i * _width + j] = rand() % 6 + 12;
			}
		}
	}

	// generate start and end rooms (3x3)
	for (int i = 18; i < 21; i++)
	{
		_levelMap[i * _width + 1] = 0;
		_levelMap[i * _width + 2] = 0;
		_levelMap[i * _width + 3] = 0;
		_levelMap[i * _width + 196] = 0;
		_levelMap[i * _width + 197] = 0;
		_levelMap[i * _width + 198] = 0;
	}

	// create a connection between start and end rooms
	while (!hasConnection())
	{
		int row = 1 + rand() % (_height - 2);
		int col = 1 + rand() % (_width - 2);

		while (_levelMap[row * _width + col] <= 0)
		{
			row = 1 + rand() % (_height - 2);
			col = 1 + rand() % (_width - 2);
		}

		_levelMap[row * _width + col] = -1 * _levelMap[row * _width + col];
		int randomSpawn = rand() % 100;
		if (randomSpawn < 10)
		{
			if (col > 4)
			{
				_actorArray->add(
					new GhostController(ActorFactory::getInstance().getActor(col * 32,
					(LevelManager::getInstance().getHeight() - row - 1) * 32, 1.0f, "Ghost", 100, ActorType::TypeNPC)));
			}
		}
		else if (randomSpawn < 13)
		{
			_actorArray->add(
				new ItemController(ActorFactory::getInstance().getActor(col * 32 + 6.0f,
				(LevelManager::getInstance().getHeight() - row - 1) * 32, 0.0f, "Ammo", 100, ActorType::TypeAmmo)));
		}
	}
}

bool LevelManager::hasConnection()
{
	int *connectionMap = new int[_width * _height];
	for (int i = 0; i < _width * _height; i++)
	{
		connectionMap[i] = 0;
	}

	for (int i = 18; i < 21; i++)
	{
		connectionMap[i * _width + 1] = 1;
		connectionMap[i * _width + 2] = 1;
		connectionMap[i * _width + 3] = 1;
	}

	DynamicArray<int> freeTiles;

	for (int i = 17; i < 22; i++)
	{
		if ((connectionMap[i * _width + 1] == 0) && (_levelMap[i * _width + 1] <= 0))
		{
			connectionMap[i * _width + 1] = 1;
			freeTiles.add(i * _width + 1);
		}
		
		if ((connectionMap[i * _width + 2] == 0) && (_levelMap[i * _width + 2] <= 0))
		{
			connectionMap[i * _width + 2] = 1;
			freeTiles.add(i * _width + 2);
		}
		
		if ((connectionMap[i * _width + 3] == 0) && (_levelMap[i * _width + 3] <= 0))
		{
			connectionMap[i * _width + 3] = 1;
			freeTiles.add(i * _width + 3);
		}

		if ((connectionMap[i * _width + 4] == 0) && (_levelMap[i * _width + 4] <= 0)
			&& (i != 17 || i != 21))
		{
			connectionMap[i * _width + 4] = 1;
			freeTiles.add(i * _width + 4);
		}
	}

	while (!freeTiles.isEmpty())
	{
		int tmpInt = freeTiles.poll();

		int row = tmpInt / _width;
		int col = tmpInt % _width;

		if (row - 1 >= 1)
		{
			if ((connectionMap[(row - 1) * _width + col] == 0) && (_levelMap[(row - 1) * _width + col] <= 0))
			{
				connectionMap[(row - 1) * _width + col] = 1;
				freeTiles.add((row - 1) * _width + col);
			}
		}

		if (row + 1 < _height - 1)
		{
			if ((connectionMap[(row + 1) * _width + col] == 0) && (_levelMap[(row + 1) * _width + col] <= 0))
			{
				connectionMap[(row + 1) * _width + col] = 1;
				freeTiles.add((row + 1) * _width + col);
			}
		}

		if (col - 1 >= 1)
		{
			if ((connectionMap[row * _width + col - 1] == 0) && (_levelMap[row * _width + col - 1] <= 0))
			{
				connectionMap[row * _width + col - 1] = 1;
				freeTiles.add(row * _width + col - 1);
			}
		}

		if (col + 1 < _width - 1)
		{
			if ((connectionMap[row * _width + col + 1] == 0) && (_levelMap[row * _width + col + 1] <= 0))
			{
				connectionMap[row * _width + col + 1] = 1;
				freeTiles.add(row * _width + col + 1);
			}
		}

		if ((row < 21) && (row >= 18) && (col < 199) && (col >= 196))
		{
			int emptiedTiles = 0;
			for (int i = 1; i < (_height - 1); i++)
			{
				for (int j = 1; j < (_width - 1); j++)
				{
					if (_levelMap[i * _width + j] <= 0)
					{
						emptiedTiles++;
					}
				}
			}

			if ((double)emptiedTiles/(double)((_height - 1) * (_width - 1)) > 0.5)
			{
				delete connectionMap;
				return true;
			}
		}
	}
	
	delete connectionMap;
	return false;
}

bool LevelManager::hasMapCollision(float x, float y, float width, float height)
{
	int col1 = (x + 1) / 32;
	int col2 = (x + width - 1) / 32;
	int row1 = 39 - (((y + 1) / 32) - 1);
	int row2 = 39 - (((y + height - 1) / 32) - 1);

	if (LevelManager::getInstance().getLevelMap()[row1 * 200 + col1] > 0)
	{
		return true;
	}

	if (LevelManager::getInstance().getLevelMap()[row2 * 200 + col1] > 0)
	{
		return true;
	}

	if (LevelManager::getInstance().getLevelMap()[row1 * 200 + col2] > 0)
	{
		return true;
	}

	if (LevelManager::getInstance().getLevelMap()[row2 * 200 + col2] > 0)
	{
		return true;
	}

	return false;
}