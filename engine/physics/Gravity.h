// Emre Can Karabacak - 10380370

#ifndef GRAVITY_H
#define GRAVITY_H

class Gravity
{
public:
	static Gravity& getInstance()
	{
		static Gravity instance;
		return instance;
	}
	static float getGravity() { return _gravity; }
private:
	Gravity() { }
	Gravity(Gravity const&);
	void operator=(Gravity const&);
	static float _gravity;
};

#endif GRAVITY_H