// Emre Can Karabacak - 2014

#include "ResourceLoader.h"

enum vElement_IDs {SkyboxE, MapE, PlayerE, UIE, MenuE, ProjectileE, ActorE, DecalsE, NumVElements};
enum vArray_IDs {SkyboxA, MapA, PlayerA, UIA, MenuA, ProjectileA, ActorA, DecalsA, NumVArrays};
enum vBuffer_IDs {SkyboxB, MapB, PlayerB, UIB, MenuB, ProjectileB, ActorB, DecalsB, NumVBuffers};
enum Texture_IDs {Empty, Castle01, Castle02, Castle03, Castle04, Castle05, Castle06, Castle07, Castle08, Castle09,
	CastleS01, CastleS02, CastleS03, CastleS04, CastleS05, CastleS06, CastleS07, CastleS08, CastleS09,
	Grass01, Ground01, Ground02, Ground03, Ground04, Ground05, Ground06, Ground07,
	GrassS01, GroundS01, GroundS02, GroundS03, GroundS04, GroundS05, GroundS06, GroundS07,
	Skybox01, Skybox02, PlayerRStill, PlayerRAir, PlayerRWalk1, PlayerRWalk2,
	PlayerLStill, PlayerLAir, PlayerLWalk1, PlayerLWalk2, HealthBar, Health, AmmoBar, AmmoBarUnit, 
	Bullet, GhostLeft, GhostRight, Ammo, PortalEntrance, PortalExit,
	MainMenuT, NewGameT, NewGameMOT, QuitGameT, QuitGameMOT, GameOverT, GameWonT,
	DistanceT, TimerT,
	Number0, Number1, Number2, Number3, Number4, Number5, Number6, Number7, Number8, Number9, NumTextures};

GLushort *skyboxIndices, *mapIndices, *playerIndices, *uiIndices, *menuIndices;
GLfloat *skyboxVertices, *mapVertices, *playerVertices, *uiVertices, *menuVertices, *projectileVertices, *ghostVertices, *portalVertices;
vec2 *skyboxTexCoords, *mapTexCoords, *playerTexCoords, *uiTexCoords, *menuTexCoords;

GLuint vPosition = 0;
GLuint vTexCoord = 0;

std::vector<unsigned char> image;
unsigned height, width;

void ResourceLoader::clearAllocations()
{
	delete skyboxIndices;
	delete mapIndices;
	delete playerIndices;
	delete uiIndices;
	delete menuIndices;
	delete skyboxVertices;
	delete mapVertices;
	delete playerVertices;
	delete uiVertices;
	delete menuVertices;
	delete projectileVertices;
	delete ghostVertices;
	delete portalVertices;
	delete skyboxTexCoords;
	delete mapTexCoords;
	delete playerTexCoords;
	delete uiTexCoords;
	delete menuTexCoords;
}

void ResourceLoader::loadAllBuffers(GLuint texShader, GLuint*& vElements, GLuint*& vArrays, GLuint*& vBuffers)
{
	// VAO[Skybox]
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vElements[SkyboxE]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * 4, skyboxIndices, GL_STATIC_DRAW);
	
	glUseProgram(texShader);
	glBindVertexArray(vArrays[SkyboxA]);
	glBindBuffer(GL_ARRAY_BUFFER, vBuffers[SkyboxB]); 
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 16 + sizeof(vec2) * 4, NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * 16, skyboxVertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 16, 
		sizeof(vec2) * 4, skyboxTexCoords);
	vPosition = glGetAttribLocation(texShader, "vPosition");
	glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(vPosition);
	vTexCoord = glGetAttribLocation(texShader, "vTexCoord");
    glVertexAttribPointer(vTexCoord, 2, GL_FLOAT, GL_FALSE, 0, ((const void*) (sizeof(GLfloat) * 16)));
	glEnableVertexAttribArray(vTexCoord);
	glUniform1i(glGetUniformLocation(texShader, "texture"), 0);
    glBindVertexArray(0);

	// VAO[Map]
	int width = LevelManager::getWidth();
	int height = LevelManager::getHeight();

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vElements[MapE]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * 4 * width * height, mapIndices, GL_STATIC_DRAW);
	
	glUseProgram(texShader);
	glBindVertexArray(vArrays[MapA]);
	glBindBuffer(GL_ARRAY_BUFFER, vBuffers[MapB]); 
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 16 * width * height + sizeof(vec2) * 4 * width * height, NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * 16 * width * height, mapVertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 16 * width * height, 
		sizeof(vec2) * 4 * width * height, mapTexCoords);
	vPosition = glGetAttribLocation(texShader, "vPosition");
	glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(vPosition);
	vTexCoord = glGetAttribLocation(texShader, "vTexCoord");
    glVertexAttribPointer(vTexCoord, 2, GL_FLOAT, GL_FALSE, 0, ((const void*) (sizeof(GLfloat) * 16 * width * height)));
	glEnableVertexAttribArray(vTexCoord);
	glUniform1i(glGetUniformLocation(texShader, "texture"), 0);
    glBindVertexArray(0);

	// VAO[Player]
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vElements[PlayerE]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * 4, playerIndices, GL_STATIC_DRAW);
	
	glUseProgram(texShader);
	glBindVertexArray(vArrays[PlayerA]);
	glBindBuffer(GL_ARRAY_BUFFER, vBuffers[PlayerB]); 
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 16 + sizeof(vec2) * 4, NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * 16, playerVertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 16, 
		sizeof(vec2) * 4, playerTexCoords);
	vPosition = glGetAttribLocation(texShader, "vPosition");
	glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(vPosition);
	vTexCoord = glGetAttribLocation(texShader, "vTexCoord");
    glVertexAttribPointer(vTexCoord, 2, GL_FLOAT, GL_FALSE, 0, ((const void*) (sizeof(GLfloat) * 16)));
	glEnableVertexAttribArray(vTexCoord);
	glUniform1i(glGetUniformLocation(texShader, "texture"), 0);
    glBindVertexArray(0);

	// VAO[UI]
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vElements[UIE]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * 29 * 4, uiIndices, GL_STATIC_DRAW);
	
	glUseProgram(texShader);
	glBindVertexArray(vArrays[UIA]);
	glBindBuffer(GL_ARRAY_BUFFER, vBuffers[UIB]); 
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 29 * 16 + sizeof(vec2) * 29 * 4, NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * 29 * 16, uiVertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 29 * 16, 
		sizeof(vec2) * 29 * 4, uiTexCoords);
	vPosition = glGetAttribLocation(texShader, "vPosition");
	glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(vPosition);
	vTexCoord = glGetAttribLocation(texShader, "vTexCoord");
    glVertexAttribPointer(vTexCoord, 2, GL_FLOAT, GL_FALSE, 0, ((const void*) (sizeof(GLfloat) * 29 * 16)));
	glEnableVertexAttribArray(vTexCoord);
	glUniform1i(glGetUniformLocation(texShader, "texture"), 0);
    glBindVertexArray(0);

	// VAO[Menu]
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vElements[MenuE]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * 3 * 4, menuIndices, GL_STATIC_DRAW);
	
	glUseProgram(texShader);
	glBindVertexArray(vArrays[MenuA]);
	glBindBuffer(GL_ARRAY_BUFFER, vBuffers[MenuB]); 
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * 16 + sizeof(vec2) * 3 * 4, NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * 3 * 16, menuVertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * 16, 
		sizeof(vec2) * 3 * 4, menuTexCoords);
	vPosition = glGetAttribLocation(texShader, "vPosition");
	glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(vPosition);
	vTexCoord = glGetAttribLocation(texShader, "vTexCoord");
    glVertexAttribPointer(vTexCoord, 2, GL_FLOAT, GL_FALSE, 0, ((const void*) (sizeof(GLfloat) * 3 * 16)));
	glEnableVertexAttribArray(vTexCoord);
	glUniform1i(glGetUniformLocation(texShader, "texture"), 0);
    glBindVertexArray(0);

	// VAO[Projectile]
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vElements[ProjectileE]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * 4, playerIndices, GL_STATIC_DRAW);
	
	glUseProgram(texShader);
	glBindVertexArray(vArrays[ProjectileA]);
	glBindBuffer(GL_ARRAY_BUFFER, vBuffers[ProjectileB]); 
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 16 + sizeof(vec2) * 4, NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * 16, projectileVertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 16, 
		sizeof(vec2) * 4, playerTexCoords);
	vPosition = glGetAttribLocation(texShader, "vPosition");
	glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(vPosition);
	vTexCoord = glGetAttribLocation(texShader, "vTexCoord");
    glVertexAttribPointer(vTexCoord, 2, GL_FLOAT, GL_FALSE, 0, ((const void*) (sizeof(GLfloat) * 16)));
	glEnableVertexAttribArray(vTexCoord);
	glUniform1i(glGetUniformLocation(texShader, "texture"), 0);
    glBindVertexArray(0);

	// VAO[Actor]
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vElements[ActorE]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * 4, playerIndices, GL_STATIC_DRAW);
	
	glUseProgram(texShader);
	glBindVertexArray(vArrays[ActorA]);
	glBindBuffer(GL_ARRAY_BUFFER, vBuffers[ActorB]); 
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 16 + sizeof(vec2) * 4, NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * 16, ghostVertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 16, 
		sizeof(vec2) * 4, playerTexCoords);
	vPosition = glGetAttribLocation(texShader, "vPosition");
	glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(vPosition);
	vTexCoord = glGetAttribLocation(texShader, "vTexCoord");
    glVertexAttribPointer(vTexCoord, 2, GL_FLOAT, GL_FALSE, 0, ((const void*) (sizeof(GLfloat) * 16)));
	glEnableVertexAttribArray(vTexCoord);
	glUniform1i(glGetUniformLocation(texShader, "texture"), 0);
    glBindVertexArray(0);

	// VAO[Decals]
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vElements[DecalsE]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * 4, playerIndices, GL_STATIC_DRAW);
	
	glUseProgram(texShader);
	glBindVertexArray(vArrays[DecalsA]);
	glBindBuffer(GL_ARRAY_BUFFER, vBuffers[DecalsB]); 
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 16 + sizeof(vec2) * 4, NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * 16, portalVertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 16, 
		sizeof(vec2) * 4, playerTexCoords);
	vPosition = glGetAttribLocation(texShader, "vPosition");
	glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(vPosition);
	vTexCoord = glGetAttribLocation(texShader, "vTexCoord");
    glVertexAttribPointer(vTexCoord, 2, GL_FLOAT, GL_FALSE, 0, ((const void*) (sizeof(GLfloat) * 16)));
	glEnableVertexAttribArray(vTexCoord);
	glUniform1i(glGetUniformLocation(texShader, "texture"), 0);
    glBindVertexArray(0);
}

void ResourceLoader::loadAllImages(GLuint*& textures)
{		
	glEnable(GL_TEXTURE_2D);
	glGenTextures(NumTextures, textures);
	glActiveTexture( GL_TEXTURE0 );
	
	loadImage(textures, "images/tiles/0.png", Empty);
	loadImage(textures, "images/tiles/1.png", Castle01);
	loadImage(textures, "images/tiles/-1.png", CastleS01);
	loadImage(textures, "images/tiles/2.png", Castle02);
	loadImage(textures, "images/tiles/-2.png", CastleS02);
	loadImage(textures, "images/tiles/3.png", Castle03);
	loadImage(textures, "images/tiles/-3.png", CastleS03);
	loadImage(textures, "images/tiles/4.png", Castle04);
	loadImage(textures, "images/tiles/-4.png", CastleS04);
	loadImage(textures, "images/tiles/5.png", Castle05);
	loadImage(textures, "images/tiles/-5.png", CastleS05);
	loadImage(textures, "images/tiles/6.png", Castle06);
	loadImage(textures, "images/tiles/-6.png", CastleS06);
	loadImage(textures, "images/tiles/7.png", Castle07);
	loadImage(textures, "images/tiles/-7.png", CastleS07);
	loadImage(textures, "images/tiles/8.png", Castle08);
	loadImage(textures, "images/tiles/-8.png", CastleS08);
	loadImage(textures, "images/tiles/9.png", Castle09);
	loadImage(textures, "images/tiles/-9.png", CastleS09);
	loadImage(textures, "images/tiles/10.png", Grass01);
	loadImage(textures, "images/tiles/-10.png", GrassS01);
	loadImage(textures, "images/tiles/11.png", Ground01);
	loadImage(textures, "images/tiles/-11.png", GroundS01);
	loadImage(textures, "images/tiles/12.png", Ground02);
	loadImage(textures, "images/tiles/-12.png", GroundS02);
	loadImage(textures, "images/tiles/13.png", Ground03);
	loadImage(textures, "images/tiles/-13.png", GroundS03);
	loadImage(textures, "images/tiles/14.png", Ground04);
	loadImage(textures, "images/tiles/-14.png", GroundS04);
	loadImage(textures, "images/tiles/15.png", Ground05);
	loadImage(textures, "images/tiles/-15.png", GroundS05);
	loadImage(textures, "images/tiles/16.png", Ground06);
	loadImage(textures, "images/tiles/-16.png", GroundS06);
	loadImage(textures, "images/tiles/17.png", Ground07);
	loadImage(textures, "images/tiles/-17.png", GroundS07);
	loadImage(textures, "images/tiles/17.png", Ground07);
	loadImage(textures, "images/skyboxes/1.png", Skybox01);
	loadImage(textures, "images/skyboxes/2.png", Skybox02);
	loadImage(textures, "images/player/right_still.png", PlayerRStill);
	loadImage(textures, "images/player/right_air.png", PlayerRAir);
	loadImage(textures, "images/player/right_walk_1.png", PlayerRWalk1);
	loadImage(textures, "images/player/right_walk_2.png", PlayerRWalk2);
	loadImage(textures, "images/player/left_still.png", PlayerLStill);
	loadImage(textures, "images/player/left_air.png", PlayerLAir);
	loadImage(textures, "images/player/left_walk_1.png", PlayerLWalk1);
	loadImage(textures, "images/player/left_walk_2.png", PlayerLWalk2);
	loadImage(textures, "images/ui/healthbar.png", HealthBar);
	loadImage(textures, "images/ui/health.png", Health);
	loadImage(textures, "images/ui/ammobar.png", AmmoBar);
	loadImage(textures, "images/ui/ammo.png", AmmoBarUnit);
	loadImage(textures, "images/ui/distance.png", DistanceT);
	loadImage(textures, "images/ui/timer.png", TimerT);
	loadImage(textures, "images/projectiles/bullet.png", Bullet);
	loadImage(textures, "images/npc/ghost_left.png", GhostLeft);
	loadImage(textures, "images/npc/ghost_right.png", GhostRight);
	loadImage(textures, "images/items/ammo.png", Ammo);
	loadImage(textures, "images/decals/portal_entrance.png", PortalEntrance);
	loadImage(textures, "images/decals/portal_exit.png", PortalExit);
	loadImage(textures, "images/menu/main_menu.png", MainMenuT);
	loadImage(textures, "images/menu/new_game.png", NewGameT);
	loadImage(textures, "images/menu/new_game_mouse_over.png", NewGameMOT);
	loadImage(textures, "images/menu/quit_game.png", QuitGameT);
	loadImage(textures, "images/menu/quit_game_mouse_over.png", QuitGameMOT);
	loadImage(textures, "images/menu/game_over.png", GameOverT);
	loadImage(textures, "images/menu/win_screen.png", GameWonT);	
	loadImage(textures, "images/numbers/0.png", Number0);
	loadImage(textures, "images/numbers/1.png", Number1);
	loadImage(textures, "images/numbers/2.png", Number2);
	loadImage(textures, "images/numbers/3.png", Number3);
	loadImage(textures, "images/numbers/4.png", Number4);
	loadImage(textures, "images/numbers/5.png", Number5);
	loadImage(textures, "images/numbers/6.png", Number6);
	loadImage(textures, "images/numbers/7.png", Number7);
	loadImage(textures, "images/numbers/8.png", Number8);
	loadImage(textures, "images/numbers/9.png", Number9);
}

void ResourceLoader::loadImage(GLuint*& textures, const char* filename, int index)
{
	image.clear();
	lodepng::decode(image, height, width, filename);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textures[index]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, height, width, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);	
	glGenerateMipmap(GL_TEXTURE_2D);
}

void ResourceLoader::loadSceneComponents()
{
	skyboxVertices = new GLfloat[16];
	skyboxVertices[0] = 0.0f;
	skyboxVertices[1] = 0.0f;
	skyboxVertices[2] = -5.0f;
	skyboxVertices[3] = 1.0f;
	skyboxVertices[4] = 500.0f;
	skyboxVertices[5] = 0.0f;
	skyboxVertices[6] = -5.0f;
	skyboxVertices[7] = 1.0f;
	skyboxVertices[8] = 500.0f;
	skyboxVertices[9] = 300.0f;
	skyboxVertices[10] = -5.0f;
	skyboxVertices[11] = 1.0f;
	skyboxVertices[12] = 0.0f;
	skyboxVertices[13] = 300.0f;
	skyboxVertices[14] = -5.0f;
	skyboxVertices[15] = 1.0f;

	skyboxIndices = new GLushort[4];
	skyboxIndices[0] = 0;
	skyboxIndices[1] = 1;
	skyboxIndices[2] = 2;
	skyboxIndices[3] = 3;

	skyboxTexCoords = new vec2[4];
	skyboxTexCoords[0] = vec2(0.0, 1.0);
	skyboxTexCoords[1] = vec2(1.0, 1.0);
	skyboxTexCoords[2] = vec2(1.0, 0.0);
	skyboxTexCoords[3] = vec2(0.0, 0.0);

	int width = LevelManager::getWidth();
	int height = LevelManager::getHeight();

	mapVertices = new GLfloat[height * width * 16];
	mapIndices = new GLushort[height * width * 4];
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			mapVertices[(i * width + j) * 16] =  (float)j * (float)32;
			mapVertices[(i * width + j) * 16 + 1] =   (float)(height - i - 1) *  (float)32;
			mapVertices[(i * width + j) * 16 + 2] =  0.0f;
			mapVertices[(i * width + j) * 16 + 3] =  1.0f;
			mapVertices[(i * width + j) * 16 + 4] =   (float)(j + 1) *  (float)32;
			mapVertices[(i * width + j) * 16 + 5] =   (float)(height - i - 1) *  (float)32;
			mapVertices[(i * width + j) * 16 + 6] =  0.0f;
			mapVertices[(i * width + j) * 16 + 7] =  1.0f;
			mapVertices[(i * width + j) * 16 + 8] =   (float)(j + 1) *  (float)32;
			mapVertices[(i * width + j) * 16 + 9] =   (float)(height - i) *  (float)32;
			mapVertices[(i * width + j) * 16 + 10] =  0.0f;
			mapVertices[(i * width + j) * 16 + 11] =  1.0f;
			mapVertices[(i * width + j) * 16 + 12] =   (float)j *  (float)32;
			mapVertices[(i * width + j) * 16 + 13] =   (float)(height - i) *  (float)32;
			mapVertices[(i * width + j) * 16 + 14] =  0.0f;
			mapVertices[(i * width + j) * 16 + 15] =  1.0f;

			mapIndices[(i * width + j) * 4] = (i * width + j) * 4;
			mapIndices[(i * width + j) * 4 + 1] = (i * width + j) * 4 + 1;
			mapIndices[(i * width + j) * 4 + 2] = (i * width + j) * 4 + 2;
			mapIndices[(i * width + j) * 4 + 3] = (i * width + j) * 4 + 3;
		}
	}
		
	mapTexCoords = new vec2[height * width * 4];
	for (int i = 0; i < height * width; i++)
	{
		mapTexCoords[i * 4] = vec2(0.0, 1.0);
		mapTexCoords[i * 4 + 1] = vec2(1.0, 1.0);
		mapTexCoords[i * 4 + 2] = vec2(1.0, 0.0);
		mapTexCoords[i * 4 + 3] = vec2(0.0, 0.0);
	}
		
	playerVertices = new GLfloat[16];

	playerVertices[0] = 0.0f;
	playerVertices[1] = 0.0f;
	playerVertices[2] = 0.0f;
	playerVertices[3] = 1.0f;
	playerVertices[4] = 12.0f;
	playerVertices[5] = 0.0f;
	playerVertices[6] = 0.0f;
	playerVertices[7] = 1.0f;
	playerVertices[8] = 12.0f;
	playerVertices[9] = 20.0f;
	playerVertices[10] = 0.0f;
	playerVertices[11] = 1.0f;
	playerVertices[12] = 0.0f;
	playerVertices[13] = 20.0f;
	playerVertices[14] = 0.0f;
	playerVertices[15] = 1.0f;

	playerIndices = new GLushort[4];
	playerIndices[0] = 0;
	playerIndices[1] = 1;
	playerIndices[2] = 2;
	playerIndices[3] = 3;

	playerTexCoords = new vec2[4];
	playerTexCoords[0] = vec2(0.0, 1.0);
	playerTexCoords[1] = vec2(1.0, 1.0);
	playerTexCoords[2] = vec2(1.0, 0.0);
	playerTexCoords[3] = vec2(0.0, 0.0);

	uiVertices = new GLfloat[29*16];
	uiVertices[0] = 5.0f;
	uiVertices[1] = 5.0f;
	uiVertices[2] = 5.0f;
	uiVertices[3] = 1.0f;
	uiVertices[4] = 165.0f;
	uiVertices[5] = 5.0f;
	uiVertices[6] = 5.0f;
	uiVertices[7] = 1.0f;
	uiVertices[8] = 165.0f;
	uiVertices[9] = 45.0f;
	uiVertices[10] = 5.0f;
	uiVertices[11] = 1.0f;
	uiVertices[12] = 5.0f;
	uiVertices[13] = 45.0f;
	uiVertices[14] = 5.0f;
	uiVertices[15] = 1.0f;
	for (int i = 1; i < 11; i++)
	{
		uiVertices[i*16 + 0] = 10.0f + (i - 1)*15.0f;
		uiVertices[i*16 + 1] = 10.0f;
		uiVertices[i*16 + 2] = 5.0f;
		uiVertices[i*16 + 3] = 1.0f;
		uiVertices[i*16 + 4] = 10.0f + i*15.0f;
		uiVertices[i*16 + 5] = 10.0f;
		uiVertices[i*16 + 6] = 5.0f;
		uiVertices[i*16 + 7] = 1.0f;
		uiVertices[i*16 + 8] = 10.0f + i*15.0f;
		uiVertices[i*16 + 9] = 40.0f;
		uiVertices[i*16 + 10] = 5.0f;
		uiVertices[i*16 + 11] = 1.0f;
		uiVertices[i*16 + 12] = 10.0f + (i - 1)*15.0f;
		uiVertices[i*16 + 13] = 40.0f;
		uiVertices[i*16 + 14] = 5.0f;
		uiVertices[i*16 + 15] = 1.0f;
	}
	uiVertices[11*16] = 335.0f;
	uiVertices[11*16 + 1] = 5.0f;
	uiVertices[11*16 + 2] = 5.0f;
	uiVertices[11*16 + 3] = 1.0f;
	uiVertices[11*16 + 4] = 495.0f;
	uiVertices[11*16 + 5] = 5.0f;
	uiVertices[11*16 + 6] = 5.0f;
	uiVertices[11*16 + 7] = 1.0f;
	uiVertices[11*16 + 8] = 495.0f;
	uiVertices[11*16 + 9] = 45.0f;
	uiVertices[11*16 + 10] = 5.0f;
	uiVertices[11*16 + 11] = 1.0f;
	uiVertices[11*16 + 12] = 335.0f;
	uiVertices[11*16 + 13] = 45.0f;
	uiVertices[11*16 + 14] = 5.0f;
	uiVertices[11*16 + 15] = 1.0f;
	for (int i = 12; i < 22; i++)
	{
		uiVertices[i*16 + 0] = 340.0f + (i - 12)*15.0f;
		uiVertices[i*16 + 1] = 10.0f;
		uiVertices[i*16 + 2] = 5.0f;
		uiVertices[i*16 + 3] = 1.0f;
		uiVertices[i*16 + 4] = 340.0f + (i - 11)*15.0f;
		uiVertices[i*16 + 5] = 10.0f;
		uiVertices[i*16 + 6] = 5.0f;
		uiVertices[i*16 + 7] = 1.0f;
		uiVertices[i*16 + 8] = 340.0f + (i - 11)*15.0f;
		uiVertices[i*16 + 9] = 40.0f;
		uiVertices[i*16 + 10] = 5.0f;
		uiVertices[i*16 + 11] = 1.0f;
		uiVertices[i*16 + 12] = 340.0f + (i - 12)*15.0f;
		uiVertices[i*16 + 13] = 40.0f;
		uiVertices[i*16 + 14] = 5.0f;
		uiVertices[i*16 + 15] = 1.0f;
	}
	uiVertices[22*16] = 170.0f;
	uiVertices[22*16 + 1] = 255.0f;
	uiVertices[22*16 + 2] = 4.0f;
	uiVertices[22*16 + 3] = 1.0f;
	uiVertices[22*16 + 4] = 330.0f;
	uiVertices[22*16 + 5] = 255.0f;
	uiVertices[22*16 + 6] = 4.0f;
	uiVertices[22*16 + 7] = 1.0f;
	uiVertices[22*16 + 8] = 330.0f;
	uiVertices[22*16 + 9] = 295.0f;
	uiVertices[22*16 + 10] = 4.0f;
	uiVertices[22*16 + 11] = 1.0f;
	uiVertices[22*16 + 12] = 170.0f;
	uiVertices[22*16 + 13] = 295.0f;
	uiVertices[22*16 + 14] = 4.0f;
	uiVertices[22*16 + 15] = 1.0f;
	uiVertices[23*16] = 170.0f;
	uiVertices[23*16 + 1] = 265.0f;
	uiVertices[23*16 + 2] = 5.0f;
	uiVertices[23*16 + 3] = 1.0f;
	uiVertices[23*16 + 4] = 182.0f;
	uiVertices[23*16 + 5] = 265.0f;
	uiVertices[23*16 + 6] = 5.0f;
	uiVertices[23*16 + 7] = 1.0f;
	uiVertices[23*16 + 8] = 182.0f;
	uiVertices[23*16 + 9] = 285.0f;
	uiVertices[23*16 + 10] = 5.0f;
	uiVertices[23*16 + 11] = 1.0f;
	uiVertices[23*16 + 12] = 170.0f;
	uiVertices[23*16 + 13] = 285.0f;
	uiVertices[23*16 + 14] = 5.0f;
	uiVertices[23*16 + 15] = 1.0f;
	uiVertices[24*16] = 405.0f;
	uiVertices[24*16 + 1] = 255.0f;
	uiVertices[24*16 + 2] = 5.0f;
	uiVertices[24*16 + 3] = 1.0f;
	uiVertices[24*16 + 4] = 495.0f;
	uiVertices[24*16 + 5] = 255.0f;
	uiVertices[24*16 + 6] = 5.0f;
	uiVertices[24*16 + 7] = 1.0f;
	uiVertices[24*16 + 8] = 495.0f;
	uiVertices[24*16 + 9] = 295.0f;
	uiVertices[24*16 + 10] = 5.0f;
	uiVertices[24*16 + 11] = 1.0f;
	uiVertices[24*16 + 12] = 405.0f;
	uiVertices[24*16 + 13] = 295.0f;
	uiVertices[24*16 + 14] = 5.0f;
	uiVertices[24*16 + 15] = 1.0f;
	uiVertices[25*16] = 405.0f;
	uiVertices[25*16 + 1] = 255.0f;
	uiVertices[25*16 + 2] = 5.0f;
	uiVertices[25*16 + 3] = 1.0f;
	uiVertices[25*16 + 4] = 430.0f;
	uiVertices[25*16 + 5] = 255.0f;
	uiVertices[25*16 + 6] = 5.0f;
	uiVertices[25*16 + 7] = 1.0f;
	uiVertices[25*16 + 8] = 430.0f;
	uiVertices[25*16 + 9] = 295.0f;
	uiVertices[25*16 + 10] = 5.0f;
	uiVertices[25*16 + 11] = 1.0f;
	uiVertices[25*16 + 12] = 405.0f;
	uiVertices[25*16 + 13] = 295.0f;
	uiVertices[25*16 + 14] = 5.0f;
	uiVertices[25*16 + 15] = 1.0f;
	uiVertices[26*16] = 425.0f;
	uiVertices[26*16 + 1] = 255.0f;
	uiVertices[26*16 + 2] = 5.0f;
	uiVertices[26*16 + 3] = 1.0f;
	uiVertices[26*16 + 4] = 450.0f;
	uiVertices[26*16 + 5] = 255.0f;
	uiVertices[26*16 + 6] = 5.0f;
	uiVertices[26*16 + 7] = 1.0f;
	uiVertices[26*16 + 8] = 450.0f;
	uiVertices[26*16 + 9] = 295.0f;
	uiVertices[26*16 + 10] = 5.0f;
	uiVertices[26*16 + 11] = 1.0f;
	uiVertices[26*16 + 12] = 425.0f;
	uiVertices[26*16 + 13] = 295.0f;
	uiVertices[26*16 + 14] = 5.0f;
	uiVertices[26*16 + 15] = 1.0f;
	uiVertices[27*16] = 450.0f;
	uiVertices[27*16 + 1] = 255.0f;
	uiVertices[27*16 + 2] = 5.0f;
	uiVertices[27*16 + 3] = 1.0f;
	uiVertices[27*16 + 4] = 475.0f;
	uiVertices[27*16 + 5] = 255.0f;
	uiVertices[27*16 + 6] = 5.0f;
	uiVertices[27*16 + 7] = 1.0f;
	uiVertices[27*16 + 8] = 475.0f;
	uiVertices[27*16 + 9] = 295.0f;
	uiVertices[27*16 + 10] = 5.0f;
	uiVertices[27*16 + 11] = 1.0f;
	uiVertices[27*16 + 12] = 450.0f;
	uiVertices[27*16 + 13] = 295.0f;
	uiVertices[27*16 + 14] = 5.0f;
	uiVertices[27*16 + 15] = 1.0f;
	uiVertices[28*16] = 470.0f;
	uiVertices[28*16 + 1] = 255.0f;
	uiVertices[28*16 + 2] = 5.0f;
	uiVertices[28*16 + 3] = 1.0f;
	uiVertices[28*16 + 4] = 495.0f;
	uiVertices[28*16 + 5] = 255.0f;
	uiVertices[28*16 + 6] = 5.0f;
	uiVertices[28*16 + 7] = 1.0f;
	uiVertices[28*16 + 8] = 495.0f;
	uiVertices[28*16 + 9] = 295.0f;
	uiVertices[28*16 + 10] = 5.0f;
	uiVertices[28*16 + 11] = 1.0f;
	uiVertices[28*16 + 12] = 470.0f;
	uiVertices[28*16 + 13] = 295.0f;
	uiVertices[28*16 + 14] = 5.0f;
	uiVertices[28*16 + 15] = 1.0f;

	uiIndices = new GLushort[29*4];
	for (int i = 0; i < 29*4; i++)
	{
		uiIndices[i] = i;
	}

	uiTexCoords = new vec2[29*4];
	for (int i = 0; i < 29; i++)
	{
		uiTexCoords[i*4] = vec2(0.0, 1.0);
		uiTexCoords[i*4 + 1] = vec2(1.0, 1.0);
		uiTexCoords[i*4 + 2] = vec2(1.0, 0.0);
		uiTexCoords[i*4 + 3] = vec2(0.0, 0.0);
	}

	menuVertices = new GLfloat[3*16];
	menuVertices[0] = 0.0f;
	menuVertices[1] = 0.0f;
	menuVertices[2] = 0.0f;
	menuVertices[3] = 1.0f;
	menuVertices[4] = 500.0f;
	menuVertices[5] = 0.0f;
	menuVertices[6] = 0.0f;
	menuVertices[7] = 1.0f;
	menuVertices[8] = 500.0f;
	menuVertices[9] = 300.0f;
	menuVertices[10] = 0.0f;
	menuVertices[11] = 1.0f;
	menuVertices[12] = 0.0f;
	menuVertices[13] = 300.0f;
	menuVertices[14] = 0.0f;
	menuVertices[15] = 1.0f;
	menuVertices[16] = 375.0f;
	menuVertices[17] = 60.0f;
	menuVertices[18] = 1.0f;
	menuVertices[19] = 1.0f;
	menuVertices[20] = 475.0f;
	menuVertices[21] = 60.0f;
	menuVertices[22] = 1.0f;
	menuVertices[23] = 1.0f;
	menuVertices[24] = 475.0f;
	menuVertices[25] = 85.0f;
	menuVertices[26] = 1.0f;
	menuVertices[27] = 1.0f;
	menuVertices[28] = 375.0f;
	menuVertices[29] = 85.0f;
	menuVertices[30] = 1.0f;
	menuVertices[31] = 1.0f;
	menuVertices[32] = 375.0f;
	menuVertices[33] = 25.0f;
	menuVertices[34] = 1.0f;
	menuVertices[35] = 1.0f;
	menuVertices[36] = 475.0f;
	menuVertices[37] = 25.0f;
	menuVertices[38] = 1.0f;
	menuVertices[39] = 1.0f;
	menuVertices[40] = 475.0f;
	menuVertices[41] = 50.0f;
	menuVertices[42] = 1.0f;
	menuVertices[43] = 1.0f;
	menuVertices[44] = 375.0f;
	menuVertices[45] = 50.0f;
	menuVertices[46] = 1.0f;
	menuVertices[47] = 1.0f;
	
	menuIndices = new GLushort[3*4];
	for (int i = 0; i < 12; i++)
	{
		menuIndices[i] = i;
	}

	menuTexCoords = new vec2[3*4];
	for (int i = 0; i < 3; i++)
	{
		menuTexCoords[i*4] = vec2(0.0, 1.0);
		menuTexCoords[i*4 + 1] = vec2(1.0, 1.0);
		menuTexCoords[i*4 + 2] = vec2(1.0, 0.0);
		menuTexCoords[i*4 + 3] = vec2(0.0, 0.0);
	}

	projectileVertices = new GLfloat[16];
	projectileVertices[0] = 0.0f;
	projectileVertices[1] = 0.0f;
	projectileVertices[2] = 2.0f;
	projectileVertices[3] = 1.0f;
	projectileVertices[4] = 3.0f;
	projectileVertices[5] = 0.0f;
	projectileVertices[6] = 2.0f;
	projectileVertices[7] = 1.0f;
	projectileVertices[8] = 3.0f;
	projectileVertices[9] = 3.0f;
	projectileVertices[10] = 2.0f;
	projectileVertices[11] = 1.0f;
	projectileVertices[12] = 0.0f;
	projectileVertices[13] = 3.0f;
	projectileVertices[14] = 2.0f;
	projectileVertices[15] = 1.0f;

	ghostVertices = new GLfloat[16];
	ghostVertices[0] = 0.0f;
	ghostVertices[1] = 0.0f;
	ghostVertices[2] = 3.0f;
	ghostVertices[3] = 1.0f;
	ghostVertices[4] = 20.0f;
	ghostVertices[5] = 0.0f;
	ghostVertices[6] = 3.0f;
	ghostVertices[7] = 1.0f;
	ghostVertices[8] = 20.0f;
	ghostVertices[9] = 20.0f;
	ghostVertices[10] = 3.0f;
	ghostVertices[11] = 1.0f;
	ghostVertices[12] = 0.0f;
	ghostVertices[13] = 20.0f;
	ghostVertices[14] = 3.0f;
	ghostVertices[15] = 1.0f;

	portalVertices = new GLfloat[16];
	portalVertices[0] = 0.0f;
	portalVertices[1] = 0.0f;
	portalVertices[2] = 0.0f;
	portalVertices[3] = 1.0f;
	portalVertices[4] = 64.0f;
	portalVertices[5] = 0.0f;
	portalVertices[6] = 0.0f;
	portalVertices[7] = 1.0f;
	portalVertices[8] = 64.0f;
	portalVertices[9] = 64.0f;
	portalVertices[10] = 0.0f;
	portalVertices[11] = 1.0f;
	portalVertices[12] = 0.0f;
	portalVertices[13] = 64.0f;
	portalVertices[14] = 0.0f;
	portalVertices[15] = 1.0f;
}