// Emre Can Karabacak - 2014

#include "BlackBoard.h"

bool BlackBoard::_gameOver = false;
bool BlackBoard::_gameWon = false;
bool BlackBoard::_isMenu = true;
bool BlackBoard::_mouseOverNewGame = false;
bool BlackBoard::_mouseOverQuitGame = false;
PlayerActor* BlackBoard::_player = 0;