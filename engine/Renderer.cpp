// Emre Can Karabacak - 2014

#include "Renderer.h"

// rotation values
GLfloat theta[] = {0.0f,0.0f,0.0f};

// player animations
int playerAnimation = 0;
int animationCount = 0;

// render and display variables
mat4 modelmat = mat4(1.0);
mat4 projmat = mat4(1.0); 

GLuint model_view_loc = 0;
GLuint proj_loc = 0;

// shaders
GLuint texShader;

// displacement values
GLfloat disX = 0.0;
GLfloat disY = 0.0;
GLfloat disZ = 0.0;

// vertex array objects and buffers
enum vElement_IDs {SkyboxE, MapE, PlayerE, UIE, MenuE, ProjectileE, ActorE, DecalsE, NumVElements};
enum vArray_IDs {SkyboxA, MapA, PlayerA, UIA, MenuA, ProjectileA, ActorA, DecalsA, NumVArrays};
enum vBuffer_IDs {SkyboxB, MapB, PlayerB, UIB, MenuB, ProjectileB, ActorB, DecalsB, NumVBuffers};
enum Texture_IDs {Empty, Castle01, Castle02, Castle03, Castle04, Castle05, Castle06, Castle07, Castle08, Castle09,
	CastleS01, CastleS02, CastleS03, CastleS04, CastleS05, CastleS06, CastleS07, CastleS08, CastleS09,
	Grass01, Ground01, Ground02, Ground03, Ground04, Ground05, Ground06, Ground07,
	GrassS01, GroundS01, GroundS02, GroundS03, GroundS04, GroundS05, GroundS06, GroundS07,
	Skybox01, Skybox02, PlayerRStill, PlayerRAir, PlayerRWalk1, PlayerRWalk2,
	PlayerLStill, PlayerLAir, PlayerLWalk1, PlayerLWalk2, HealthBar, Health, AmmoBar, AmmoBarUnit, 
	Bullet, GhostLeft, GhostRight, Ammo, PortalEntrance, PortalExit,
	MainMenuT, NewGameT, NewGameMOT, QuitGameT, QuitGameMOT, GameOverT, GameWonT,	
	DistanceT, TimerT,
	Number0, Number1, Number2, Number3, Number4, Number5, Number6, Number7, Number8, Number9, NumTextures};

GLuint* vElements = new GLuint[NumVElements];
GLuint* vArrays = new GLuint[NumVArrays];
GLuint* vBuffers = new GLuint[NumVBuffers];
GLuint* textures = new GLuint[NumTextures];

void Renderer::initiateRenderer()
{
	// load resources
	ResourceLoader::getInstance().loadAllImages(textures);

	// load scene components
	ResourceLoader::getInstance().loadSceneComponents();

	// shaders
	ShaderInfo texShaders[] = {
		{GL_VERTEX_SHADER, "shaders/vshader_texture.glsl"},
		{GL_FRAGMENT_SHADER, "shaders/fshader_texture.glsl"},
		{GL_NONE, NULL}
	};	
	
	texShader = LoadShaders(texShaders);
	
	// set up buffers and vertex arrays
	glGenBuffers(NumVBuffers, vBuffers);	
	glGenVertexArrays(NumVArrays, vArrays);
	glGenBuffers(NumVElements, vElements);	

	ResourceLoader::getInstance().loadAllBuffers(texShader, vElements, vArrays, vBuffers);

	// free up the memory that isn't used any longer
	ResourceLoader::getInstance().clearAllocations();
}

void Renderer::updateRenderer()
{
	Point* p = BlackBoard::getInstance().getPlayer()->getLocation();

	disX = -p->getX()+250;
	disY = -p->getY()+150;
}

void Renderer::renderScene()
{
	glClearColor (0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	projmat = Ortho(VIEW_LEFT, VIEW_RIGHT, VIEW_BOTTOM, VIEW_TOP, VIEW_NEAR, VIEW_FAR);
	projmat = projmat * Translate(disX, disY, 0.0);

	if (BlackBoard::getInstance().isMenu())
	{
		drawMenu();
		return;
	}

	drawSkybox();
	drawMap();
	drawDecals();
	drawPlayer();

	DynamicArray<Controller*>* actorArray = LevelManager::getInstance().getActorArray();

	for (int i = 0; i < actorArray->size(); i++)
	{
		if (actorArray[0][i]->getActor()->getType() == ActorType::TypeProjectile)
		{
			drawProjectile(actorArray[0][i]->getActor());
		}
		else
		{
			drawActor(actorArray[0][i]->getActor());
		}
	}
	
	drawUI();
}

void Renderer::drawUI()
{
	glUseProgram(texShader);

	glEnable(GL_TEXTURE_2D);	
	glActiveTexture( GL_TEXTURE0 );	
	
	glBindVertexArray(vArrays[UIA]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vElements[UIE]);

	int timeLeft = LevelManager::getInstance().getSecondsLeft();
	int minutes = timeLeft / 60;
	int secs = timeLeft % 60;

	for (int i = 0; i < 29; i++)
	{
		modelmat = mat4(1.0);
		modelmat = modelmat * Translate(-disX, -disY, 0.0);
		
		proj_loc = glGetUniformLocation(texShader, "projection");
		model_view_loc = glGetUniformLocation(texShader, "modelview");
		glUniformMatrix4fv(proj_loc, 1, GL_TRUE, projmat);
		glUniformMatrix4fv(model_view_loc, 1, GL_TRUE, modelmat);

		if (i == 0)
		{
			glBindTexture(GL_TEXTURE_2D, textures[HealthBar]);
		}
		else if (i < 11)
		{
			if (UIManager::getInstance().getHealth() > (i - 1) * 10)
			{
				glBindTexture(GL_TEXTURE_2D, textures[Health]);
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D, textures[Empty]);
			}
		}
		else if (i == 11)
		{
			glBindTexture(GL_TEXTURE_2D, textures[AmmoBar]);
		}
		else if (i < 22)
		{
			if (UIManager::getInstance().getAmmo() > (i - 12))
			{
				glBindTexture(GL_TEXTURE_2D, textures[AmmoBarUnit]);
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D, textures[Empty]);
			}
		}
		else if (i == 22)
		{
			glBindTexture(GL_TEXTURE_2D, textures[DistanceT]);
		}
		else if (i == 23)
		{
			int playerDistance = BlackBoard::getInstance().getPlayer()->getLocation()->getX() 
				/ ((float)32*LevelManager::getInstance().getWidth()) * 150.0f;

			modelmat = modelmat * Translate(playerDistance, 0.0, 0.0);
			glUniformMatrix4fv(model_view_loc, 1, GL_TRUE, modelmat);

			glBindTexture(GL_TEXTURE_2D, textures[PlayerRStill]);
		}
		else if (i == 24)
		{			
			glBindTexture(GL_TEXTURE_2D, textures[TimerT]);
		}
		else if (i == 25)
		{			
			glBindTexture(GL_TEXTURE_2D, textures[Number0 + minutes / 10]);
		}
		else if (i == 26)
		{			
			glBindTexture(GL_TEXTURE_2D, textures[Number0 + minutes % 10]);
		}
		else if (i == 27)
		{			
			glBindTexture(GL_TEXTURE_2D, textures[Number0 + secs / 10]);
		}
		else if (i == 28)
		{			
			glBindTexture(GL_TEXTURE_2D, textures[Number0 + secs % 10]);
		}
		glDrawElements(GL_QUADS, 4, GL_UNSIGNED_SHORT, ((const void*) (sizeof(GLushort) * i * 4)));
	}

	glDisable(GL_TEXTURE_2D);

	glUseProgram(0);
	glBindVertexArray(0);
}

void Renderer::drawMenu()
{
	glUseProgram(texShader);

	modelmat = mat4(1.0);
	modelmat = modelmat * Translate(-disX, -disY, 0.0);
	
	proj_loc = glGetUniformLocation(texShader, "projection");
	model_view_loc = glGetUniformLocation(texShader, "modelview");
	glUniformMatrix4fv(proj_loc, 1, GL_TRUE, projmat);
	glUniformMatrix4fv(model_view_loc, 1, GL_TRUE, modelmat);

	glEnable(GL_TEXTURE_2D);	
	glActiveTexture( GL_TEXTURE0 );	
	
	glBindVertexArray(vArrays[MenuA]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vElements[MenuE]);
	for (int i = 0; i < 3; i++)
	{
		if (i == 0)
		{
			if (BlackBoard::getInstance().isGameOver())
			{
				if (BlackBoard::getInstance().isGameWon())
				{
					glBindTexture(GL_TEXTURE_2D, textures[GameWonT]);
				}
				else
				{
					glBindTexture(GL_TEXTURE_2D, textures[GameOverT]);
				}
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D, textures[MainMenuT]);
			}
		}
		else if (i == 1)
		{
			if (BlackBoard::getInstance().isMouseONG())
			{
				glBindTexture(GL_TEXTURE_2D, textures[NewGameMOT]);
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D, textures[NewGameT]);
			}
		}
		else if (i == 2)
		{
			if (BlackBoard::getInstance().isMouseOQG())
			{
				glBindTexture(GL_TEXTURE_2D, textures[QuitGameMOT]);
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D, textures[QuitGameT]);
			}
		}
		glDrawElements(GL_QUADS, 4, GL_UNSIGNED_SHORT, ((const void*) (sizeof(GLushort) * i * 4)));
	}

	glDisable(GL_TEXTURE_2D);

	glUseProgram(0);
	glBindVertexArray(0);
}

void Renderer::drawDecals()
{
	glUseProgram(texShader);
	
	glEnable(GL_TEXTURE_2D);	
	glActiveTexture(GL_TEXTURE0);	
	
	glBindVertexArray(vArrays[DecalsA]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vElements[DecalsE]);

	glBindTexture(GL_TEXTURE_2D, textures[PortalEntrance]);
	modelmat = mat4(1.0);
	modelmat = modelmat*Translate(32 + 16, 32 * 19, 0.0f);

	proj_loc = glGetUniformLocation(texShader, "projection");
	model_view_loc = glGetUniformLocation(texShader, "modelview");
	glUniformMatrix4fv(proj_loc, 1, GL_TRUE, projmat);
	glUniformMatrix4fv(model_view_loc, 1, GL_TRUE, modelmat);
		
	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_SHORT, NULL);

	glBindTexture(GL_TEXTURE_2D, textures[PortalExit]);
	modelmat = mat4(1.0);
	modelmat = modelmat*Translate(32 * 196 + 16, 32 * 19, 0.0f);

	proj_loc = glGetUniformLocation(texShader, "projection");
	model_view_loc = glGetUniformLocation(texShader, "modelview");
	glUniformMatrix4fv(proj_loc, 1, GL_TRUE, projmat);
	glUniformMatrix4fv(model_view_loc, 1, GL_TRUE, modelmat);
		
	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_SHORT, NULL);


	glDisable(GL_TEXTURE_2D);

	glUseProgram(0);
	glBindVertexArray(0);
}

void Renderer::drawPlayer()
{
	glUseProgram(texShader);

	modelmat = mat4(1.0);
	PlayerActor* player = BlackBoard::getInstance().getPlayer();
	Point* p = player->getLocation();
	modelmat = modelmat*Translate(p->getX(),p->getY(),0.0f);
	
	proj_loc = glGetUniformLocation(texShader, "projection");
	model_view_loc = glGetUniformLocation(texShader, "modelview");
	glUniformMatrix4fv(proj_loc, 1, GL_TRUE, projmat);
	glUniformMatrix4fv(model_view_loc, 1, GL_TRUE, modelmat);

	glEnable(GL_TEXTURE_2D);	
	glActiveTexture( GL_TEXTURE0 );	
	
	glBindVertexArray(vArrays[PlayerA]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vElements[PlayerE]);

	if (player->getPlayerDirection() == 0)
	{
		if (player->getPlayerAir() == 1)
		{
			glBindTexture(GL_TEXTURE_2D, textures[PlayerLAir]);
		}
		else if (player->getPlayerMoving() == 1)
		{
			if (playerAnimation == 0)
			{
				glBindTexture(GL_TEXTURE_2D, textures[PlayerLWalk1]);
				animationCount++;
				if (animationCount == 5)
				{
					playerAnimation = 1 - playerAnimation;
					animationCount = 0;
				}
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D, textures[PlayerLWalk2]);
				animationCount++;
				if (animationCount == 5)
				{
					playerAnimation = 1 - playerAnimation;
					animationCount = 0;
				}
			}
		}
		else
		{
			glBindTexture(GL_TEXTURE_2D, textures[PlayerLStill]);
		}
	}
	else
	{
		if (player->getPlayerAir() == 1)
		{
			glBindTexture(GL_TEXTURE_2D, textures[PlayerRAir]);
		}
		else if (player->getPlayerMoving() == 1)
		{
			if (playerAnimation == 0)
			{
				glBindTexture(GL_TEXTURE_2D, textures[PlayerRWalk1]);
				animationCount++;
				if (animationCount == 5)
				{
					playerAnimation = 1 - playerAnimation;
					animationCount = 0;
				}
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D, textures[PlayerRWalk2]);
				animationCount++;
				if (animationCount == 5)
				{
					playerAnimation = 1 - playerAnimation;
					animationCount = 0;
				}
			}
		}
		else
		{
			glBindTexture(GL_TEXTURE_2D, textures[PlayerRStill]);
		}
	}
	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_SHORT, NULL);

	glDisable(GL_TEXTURE_2D);

	glUseProgram(0);
	glBindVertexArray(0);
}

void Renderer::drawActor(Actor* actor)
{
	glUseProgram(texShader);
	
	glEnable(GL_TEXTURE_2D);	
	glActiveTexture( GL_TEXTURE0 );	
	
	glBindVertexArray(vArrays[ActorA]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vElements[ActorE]);

	modelmat = mat4(1.0);
	Point* p = actor->getLocation();
	modelmat = modelmat*Translate(p->getX(),p->getY(),0.0f);

	if (actor->getType() == ActorType::TypeNPC)
	{
		Velocity* v = actor->getVelocity();
		if (v->getHSpeed() >= 0)
		{
			glBindTexture(GL_TEXTURE_2D, textures[GhostRight]);
		}
		else
		{
			glBindTexture(GL_TEXTURE_2D, textures[GhostLeft]);
		}
	}
	else if (actor->getType() == ActorType::TypeAmmo)
	{
		glBindTexture(GL_TEXTURE_2D, textures[Ammo]);
	}

	proj_loc = glGetUniformLocation(texShader, "projection");
	model_view_loc = glGetUniformLocation(texShader, "modelview");
	glUniformMatrix4fv(proj_loc, 1, GL_TRUE, projmat);
	glUniformMatrix4fv(model_view_loc, 1, GL_TRUE, modelmat);
	
	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_SHORT, NULL);

	glDisable(GL_TEXTURE_2D);

	glUseProgram(0);
	glBindVertexArray(0);
}

void Renderer::drawProjectile(Actor* actor)
{
	glUseProgram(texShader);
	
	glEnable(GL_TEXTURE_2D);	
	glActiveTexture(GL_TEXTURE0);	
	
	glBindVertexArray(vArrays[ProjectileA]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vElements[ProjectileE]);

	glBindTexture(GL_TEXTURE_2D, textures[Bullet]);

	modelmat = mat4(1.0);
	Point* p = actor->getLocation();
	modelmat = modelmat*Translate(p->getX(),p->getY(),0.0f);

	proj_loc = glGetUniformLocation(texShader, "projection");
	model_view_loc = glGetUniformLocation(texShader, "modelview");
	glUniformMatrix4fv(proj_loc, 1, GL_TRUE, projmat);
	glUniformMatrix4fv(model_view_loc, 1, GL_TRUE, modelmat);
		
	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_SHORT, NULL);
	
	glDisable(GL_TEXTURE_2D);

	glUseProgram(0);
	glBindVertexArray(0);
}

void Renderer::drawSkybox()
{	
	glUseProgram(texShader);

	modelmat = mat4(1.0);
	modelmat = modelmat * Translate(-disX, -disY, 0.0);
	
	proj_loc = glGetUniformLocation(texShader, "projection");
	model_view_loc = glGetUniformLocation(texShader, "modelview");
	glUniformMatrix4fv(proj_loc, 1, GL_TRUE, projmat);
	glUniformMatrix4fv(model_view_loc, 1, GL_TRUE, modelmat);

	glEnable(GL_TEXTURE_2D);	
	glActiveTexture( GL_TEXTURE0 );	
	
	glBindVertexArray(vArrays[SkyboxA]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vElements[SkyboxE]);
	if (LevelManager::getSkybox() == 1)
	{
		glBindTexture(GL_TEXTURE_2D, textures[Skybox01]);
	}
	else
	{
		glBindTexture(GL_TEXTURE_2D, textures[Skybox02]);
	}
	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_SHORT, NULL);

	glDisable(GL_TEXTURE_2D);

	glUseProgram(0);
	glBindVertexArray(0);
}

void Renderer::drawMap()
{
	glUseProgram(texShader);

	modelmat = mat4(1.0);
	
	proj_loc = glGetUniformLocation(texShader, "projection");
	model_view_loc = glGetUniformLocation(texShader, "modelview");
	glUniformMatrix4fv(proj_loc, 1, GL_TRUE, projmat);
	glUniformMatrix4fv(model_view_loc, 1, GL_TRUE, modelmat);

	glEnable(GL_TEXTURE_2D);	
	glActiveTexture( GL_TEXTURE0 );	
	
	glBindVertexArray(vArrays[MapA]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vElements[MapE]);
	for (int i = 0; i < LevelManager::getHeight() * LevelManager::getWidth(); i++)
	{
		int tileTex = LevelManager::getLevelMap()[i];
		if (tileTex >= 0 && tileTex < 10)
		{
			glBindTexture(GL_TEXTURE_2D, textures[tileTex]);
		}
		else if (tileTex >= 10)
		{
			glBindTexture(GL_TEXTURE_2D, textures[tileTex + 9]);
		}
		else if (tileTex > -10)
		{
			glBindTexture(GL_TEXTURE_2D, textures[-tileTex + 9]);
		}
		else if (tileTex > -20)
		{
			glBindTexture(GL_TEXTURE_2D, textures[-tileTex + 17]);
		}
		glDrawElements(GL_QUADS, 4, GL_UNSIGNED_SHORT, ((const void*) (sizeof(GLushort) * i * 4)));
	}

	glDisable(GL_TEXTURE_2D);

	glUseProgram(0);
	glBindVertexArray(0);
}
