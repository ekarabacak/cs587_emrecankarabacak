// Emre Can Karabacak - 10380370

#ifndef UIMANAGER_H
#define UIMANAGER_H

class UIManager
{
public:
	static UIManager& getInstance()
	{
		static UIManager instance;
		return instance;
	}
	static void setHealth(int health) { _healthBar = health; }
	static int getHealth() { return _healthBar; }
	static void setAmmo(int ammo) { _ammoBar = ammo; }
	static int getAmmo() { return _ammoBar; }
private:
	UIManager() { }
	UIManager(UIManager const&);
	void operator=(UIManager const&);
	static int _healthBar;
	static int _ammoBar;
};

#endif