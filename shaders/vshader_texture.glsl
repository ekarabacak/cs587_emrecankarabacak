#version 330 core

uniform mat4 projection;
uniform mat4 modelview;

attribute vec2 vTexCoord;
attribute vec4 vPosition;

out vec2 texCoord;

void main() 
{ 
	texCoord    = vTexCoord;
	gl_Position = projection*modelview*vPosition;
}