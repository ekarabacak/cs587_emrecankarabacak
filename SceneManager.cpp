// Emre Can Karabacak - 2014

#include "SceneManager.h"

using std::cout;
using std::endl;
using std::string;
using std::vector;

// player movement input
int up = 0;
int down = 0;
int right = 0;
int left = 0;

// menu variables
bool mouseOverNewGame = false;
bool mouseOverQuitGame = false;

// json object
DynamicArray<JSONObject>* jsonLevel;

SceneManager::SceneManager()
{
	srand(time(NULL));
	_quitGame = false;
	_isMenu = true;
	initScene();
	initData();
}

void SceneManager::initData()
{
	// initiate renderer and resources
	Renderer::getInstance().initiateRenderer();
}

void SceneManager::initScene()
{
	_gameOver = false;
	_gameWon = false;

	// initiate the JSONReader
	jsonLevel = jsonReader.readJSON("data/level1.eckd");

	LevelManager::getInstance().initializeLevel(jsonLevel);
	
	_player = new PlayerController(ActorFactory::getInstance().getActor(64.0, 640.0f, 1.0f, "Player", 100, ActorType::TypePlayer));	
	BlackBoard::getInstance().updatePlayer(_player->getActor());
	UIManager::getInstance().setAmmo(10);
}

void SceneManager::tick(float dT)
{
	if (_isMenu)
	{
		BlackBoard::getInstance().setMouseONG(mouseOverNewGame);
		BlackBoard::getInstance().setMouseOQG(mouseOverQuitGame);
		return;
	}

	Renderer::getInstance().updateRenderer();

	UIManager::getInstance().setHealth(_player->getActor()->getHealth());
	
	DynamicArray<Controller*>* actorArray = LevelManager::getInstance().getActorArray();

	LevelManager::getInstance().updateActorMap();
	LevelManager::getInstance().updateTime(dT);

	// player updates
	applyMovementInput();
	_player->tick(dT);
	checkWinCondition();

	for (int i = 0; i < LevelManager::getInstance().getActorArray()->size(); i++)
	{
		actorArray[0][i]->tick(dT);
	}

	for (int i = LevelManager::getInstance().getActorArray()->size() - 1; i >= 0; i--)
	{
		if (actorArray[0][i]->getActor()->isDead())
		{
			actorArray->removeAt(i);
		}
	}

	if (_player->getActor()->isDead())
	{
		_gameOver = true;
		_isMenu = true;
	}	

	if (LevelManager::getInstance().getSecondsLeft() == 0)
	{
		_gameOver = true;
		_isMenu = true;
	}

	BlackBoard::getInstance().setGameOver(_gameOver);
	BlackBoard::getInstance().setGameWon(_gameWon);
	BlackBoard::getInstance().setMenu(_isMenu);
	BlackBoard::getInstance().updatePlayer(_player->getActor());
}

void SceneManager::drawScene()
{
	Renderer::getInstance().renderScene();
}

void SceneManager::handleEvent(SDL_Event *event)
{	
	Uint32 eventType = event->type;

	if (eventType == SDL_KEYDOWN)
	{
		SDL_KeyboardEvent kEvent = event->key;

		if (kEvent.keysym.sym == SDLK_w)
		{
			up = 1;
		}
		
		if (kEvent.keysym.sym == SDLK_s)
		{
			down = 1;
		}
		
		if (kEvent.keysym.sym == SDLK_d)
		{
			right = 1;
		}

		if (kEvent.keysym.sym == SDLK_a)
		{
			left = 1;
		}
	}
	else if (eventType == SDL_KEYUP)
	{
		SDL_KeyboardEvent kEvent = event->key;

		if (kEvent.keysym.sym == SDLK_w)
		{
			up = 0;
		}
		
		if (kEvent.keysym.sym == SDLK_s)
		{
			down = 0;
		}
		
		if (kEvent.keysym.sym == SDLK_d)
		{
			right = 0;
		}

		if (kEvent.keysym.sym == SDLK_a)
		{
			left = 0;
		}
	}
	else if (eventType == SDL_MOUSEBUTTONDOWN)
	{
		SDL_MouseButtonEvent* mouseEvent = (SDL_MouseButtonEvent*)event;

		if (mouseEvent->button == SDL_BUTTON_LEFT)
		{
			if (_isMenu)
			{
				int x = mouseEvent->x;
				int y = mouseEvent->y;

				if (x >= 750 && x <= 950 && y >= 450 && y <= 480)
				{
					if (_gameOver)
					{
						delete LevelManager::getInstance().getActorArray();
						delete LevelManager::getInstance().getActorMap();
						initScene();
					}
					_isMenu = false;
				}

				if (x >= 750 && x <= 950 && y >= 500 && y <= 550)
				{
					_quitGame = true;
				}
			}
			else
			{
				int ammo = UIManager::getInstance().getAmmo();
				if (ammo > 0)
				{
					float x = (float)mouseEvent->x - 512.0f;
					float y = 280.0f - (float)mouseEvent->y;

					float xSpeed = 10.0f * (x / (abs(x) + abs(y)));
					float ySpeed = 10.0f * (y / (abs(x) + abs(y)));

					Point* p = _player->getActor()->getLocation();

					ProjectileController* nProj = new ProjectileController(
						ActorFactory::getInstance().getActor(p->getX()+6.0f, p->getY()+10.0f, p->getZ(), "Projectile",
						1, ActorType::TypeProjectile));

					nProj->getActor()->getVelocity()->setHSpeed(xSpeed);
					nProj->getActor()->getVelocity()->setVSpeed(ySpeed);

					LevelManager::getInstance().getActorArray()->add(nProj);

					UIManager::getInstance().setAmmo(ammo - 1);
				}
			}
		}
	}

	if (_isMenu)
	{
		int x;
		int y;
		SDL_GetMouseState(&x, &y);

		if (x >= 750 && x <= 950 && y >= 450 && y <= 480)
		{
			mouseOverNewGame = true;
		}
		else
		{
			mouseOverNewGame = false;
		}

		if (x >= 750 && x <= 950 && y >= 500 && y <= 550)
		{
			mouseOverQuitGame = true;
		}
		else
		{
			mouseOverQuitGame = false;
		}
	}
}

void SceneManager::applyMovementInput()
{
	Velocity* pVel = _player->getActor()->getVelocity();

	if (right - left > 0)
	{
		_player->getActor()->setPlayerMoving(1);
		_player->getActor()->setPlayerDirection(1);
		pVel->setHSpeed(5.0f);
	}
	else if (right - left < 0)
	{
		_player->getActor()->setPlayerMoving(1);
		_player->getActor()->setPlayerDirection(0);
		pVel->setHSpeed(-5.0f);
	}
	else
	{		
		_player->getActor()->setPlayerMoving(0);
		pVel->setHSpeed(0.0f);
	}

	if (up - down > 0)
	{		
		_player->getActor()->setPlayerAir(1);
		pVel->setVSpeed(5.0f);
	}
	else if (up - down < 0)
	{
		_player->getActor()->setPlayerAir(0);
		pVel->setVSpeed(-5.0f);
	}
	else
	{
		_player->getActor()->setPlayerAir(0);
	}
}

void SceneManager::checkWinCondition()
{
	Point* pLoc = _player->getActor()->getLocation();

	float pX = pLoc->getX();
	float pY = pLoc->getY();

	if (pX >= 32 * 196 + 16 && pX <= 32 * 196 + 80 && pY >= 32 * 19 && pY <= 32 * 19 + 64)
	{
		if (!_player->getActor()->isDead())
		{
			_gameWon = true;
			_gameOver = true;
			_isMenu = true;
		}
	}
}

SceneManager::~SceneManager()
{

}