# README #

This is the repository for my CS 587 - Game Engine Design project, which is a 2D platformer called The Stronghold.

Version Alpha 0.442

To be able to run you should have the latest C++ redistributables from Microsoft.

Use W, A, S and D for movement. And mouse button to shoot a projectile that kills enemies or destroys blocks.

Objective is to reach the portal at the end without dying and before timer runs out.

Enjoy.